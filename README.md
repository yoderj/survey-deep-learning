# Survey of Deep Learning

## Introduction

This is the public repository for the course, Survey of Deep Learning.

This repository includes draft material for the course.

[The first canvas offering of this course](https://msoe.instructure.com/courses/17792) is available to students taking this course.

A box folder for past exams and quizzes this term, [CSC4980Y Deep Learning Yoder](https://msoe.app.box.com/folder/248373098344), is available to students currently taking the course.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
Students currently taking the course are welcome to contribute to the course.  Please feel free to [make merge requests](https://gitlab.com/yoderj/survey-deep-learning/-/merge_requests/new) or simply send me emails or chats through MS Teams.

## Building pages from markdown
The pages of this wiki are built in markdown.  The current markdown builder is pandoc, as I (Josiah) like pandoc's `$...$` style for noting equations and its default "blog-style" format for a self-contained page (the `-s` option).

Install pandoc.

Once you have installed pandoc, simply run the command at the top of the `*.md` file for which you want a `*.html` file to be produced.  Then, copy the HTML source into the canvas assignment's HTML source manually.

For labs with equations the default conversion is simply:

```
pandoc -s --mathjax index.md > index.html # Regenerate the .html output
```

And for other .md files you can use:
```bash
stem=index; pandoc -s --mathjax $stem.md > $stem.html # Regenerate the .html output
```
These commands are safe to run, as we consider .html as "compiler output" only in this repository, as discussed in the next section.

## Generating markdown from HTML
This repo does not store any .html files. Instead, it stores markdown files in html.

If you would like to convert an existing HTML file into the format used by this repo, you can use this command, editing stem to capture the part of the filename before the .html in your existing file:

```
stem=index; if [ ! -e $stem.md ]; then pandoc $stem.html -o $stem.md; else echo "Output file $stem.md exists"; fi
```

*THIS IS DESIGNED to NOT overwrite an existing markdown file*, since you only need this when adding content to the repo that doesn't exist yet.  To render existing content in the repo, please see the next section, below: 

When the source HTML includes background-color markup, you can use the vi command
```
:%s_\[\|\(\]{.*}\)__gc
```
To find and (if desirable) remove this markup from the .markdown version.

## Authors
* Josiah Yoder

## License
CC-BY-SA
