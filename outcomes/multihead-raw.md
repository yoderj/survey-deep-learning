# Achieved Outcomes

* Explain why multi-headed attention is critical to the transformer architecture (instructor-added)
* (a) Sketch the Multi-Head Attention layer.
* (b) Explain what are dimensions of each block in Multi-Head Attention based on given number of heads h.
* (c) Describe how are the outputs of different heads connected after they leave Scaled Dot-Product Attention part.
* Describe why multi-heads are used
* Explain why there are linear layers after the multi-heads
* Explain how each head is concentrated back into each other
* Provide justification as to why the dimensionality of the inputs to the multi-head attention is reduced rather than maintaining their original size.
* Write pseudocode for single and multi-head attention.
* Describe how the compute required for the transformer was able to remain relatively the same even thought seven additional attention heads were added.
* Explain why each head results in a different output.
* Write the formulas that relate to multiheaded attention.
* Explain why we use multiheaded attention is used over a single head.
* Explain why transformers utilize multiple heads with attention.
* Explain the purpose of the concatenation function.
* Identify matrix shapes given proper information.
* Explain the idea of using multiple heads and the reasoning behind it
* Describe what concat does
* Explain the importance of the input linear layers
* Describe the dimensionality of the output.
* Describe the benefit of multiheaded attention.
* Describe where values come from for the dimensionality of the output.
* Explain the purpose of attention layer
* Explain why they need to concatenate
* Do the multiplications needed to complete this layer
* Implement multihead attention in code by following the examples from d2l.ai.
* Explain how differences in dimensionality will affect QKV values and how model and head size will change.
* Explain how post-concatenation values are reached in the attention process.

# Not-yet-complete Outcomes

## Discussed in class
I believe this was answered in lecture after the reading was complete

* Explain why each of the heads learn something different rather than all learning the same feature

## Unclassified
* Explain why there is no activation functions applied to multiheaded attention
* (a) Explain the purpose of W O matrix after concatenation.
* (b) Describe how is this possible: ”Due to the reduced dimension of each
* head, the total computational cost is similar to that of single-head
* attention with full dimensionality.”
* Explain how each head work independently
* Explain how weights are distributed
* Identify when to use multi-head attention and when it won’t be as effective.
* Describe the shape of the head_i for each of the individual heads
* Explain how the heads are kept from learning the same things
* Describe the implementation of the Multiheaded Attention
* Learn to interpret dimensionality easier
* Better understand the work behind the attention layer
* Write out an example of an attention layer, similar to the pytorch we went through in class
* I can implement multihead attention in code by following the examples from d2l.ai.
* Fully compute the attention equations.
* Write optimization methods for the attention or softmax calculations would be important, such as finding 1/sqrt(d(k)) or like optimizations.

# Why Linear Embeddings are needed
Instructor remarks:

I finally get it!  Something in your outcomes made this click for me! 

When a network uses a residual structure, the meaning of each portion of the embedding is carried forward from the input of the section to the output, with only minor adjustments to the embedding made by each layer.

The linear layer that follows the softmax embedding is necessary to allow the head embeddings to map onto the different parts of the full embedding related to their work.  For example, the linear embedding might simply route the output of a grammar head that determines a part of speach to a part-of-speech region of the embedding.  (In practice, it will be hard to see this, because "part of speach" is likely to be a direction within the embedded space rather than a few adjacent elements of the embedding.)

As many of you said, it allows the outputs of the different heads to be "blended" into a single head.  Yet it is a bit more than blending.  It is a redirecting into an existing latent space.
