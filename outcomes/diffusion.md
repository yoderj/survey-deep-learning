
For the basics of diffusion models, these are the best resources I've found so far:

* https://lilianweng.github.io/posts/2021-07-11-diffusion-models/ -- a blog post that works through equations and includes helpful figures.
* https://arxiv.org/pdf/1503.03585.pdf -- main paper used in class -- " 
* https://arxiv.org/pdf/2209.00796v8.pdf -- survey paper

Beyond scope:

* http://yang-song.net/blog/2021/score/ -- A blog-post on score-based models.  Recommended by the earlier blog post, but this one takes the score-based appraoch rather than the "diffusion models" approach. Both approaches lead to the same backprop equations.

