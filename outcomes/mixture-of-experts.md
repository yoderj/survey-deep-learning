This is a placeholder for future references and outcomes related to the mixture of experts layer.

# (Optional) Beyond requirements
* *Note* that the Grok-1 model contains very few components beyond the standard components we have discussed so far -- mixture of experts, multihead attention, layers that repeat, simple embedding of tokens, normalization, etc. [Github for Grok-1: model.py](https://github.com/xai-org/grok-1/blob/main/model.py)
* *Note* some of the extra features we have not discussed that are used in Grok-1:
  * Explicit memory management
  * Explicit distribution across multiple processing units
  * Relative as well as absolute positional encoding: \[[RoPE embeddings](https://arxiv.org/pdf/2104.09864.pdf)\]

