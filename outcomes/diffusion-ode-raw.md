Questions asked:

* Describe the responsibility of the denoise() function used in the first code sample of this post and illustrated in Fig. 2 of the blogpost
* Describe the relationship between the squiggly lines and the smooth gradients in Figure 4. (an animated figure)
  * Example correct answer: "The lines are the diffusion paths for individual images whereas the smooth gradients are the distribution of diffusion paths"
* Describe the difference between the denoising algorithm illustrated in Figure 5 and Figure 6.
* Describe how the black arrows in, e.g., Figure 10, relate to the denoise() algorithm


Brief sketch of what we discussed in Week 15 Monday class:

* SDE vs ODE -- noise added during diffusion vs. noise NOT added during diffusion
* Non-noisy version: One noisy image maps to one de-noised image
* What the network learns: To simply recover the image to which noise was added
* The simple algorithm: Blended average of denoised and noised image.


Raw student-achieved outcomes:

* Explain what denoising is.
* Explain the tradeoffs when choosing a large or small step size.
* Explain what the uniform gradient is in the graph.
* 1. Given an image that is total noise, describe the process of producing a denoised image.
* 2. Describe the function of a noise schedule, why can we not use a fixed difference of noise between steps?
* 3. At a high level explain the difference between stochastic denoising and deterministic denoising
* 1) Describe how changing the noise schedule can change the way the curvature of flow lines
* 2) Describe why using prediction on low noise levels is good verse high noise levels is bad.
* 3) Explain what the Heun step is and why it is used.
* a. Explain the purpose of the denoise function.
* b. Explain how an ODE can improve the denoising model.
* c. Explain how noise schedules can improve an ODE.
* 1. Describe how the denoise algorithm works to generate images from noise.
* 2. Describe the difference in algorithms that traverse gradients.
* 3. Describe how the lines created in diagrams are generated.
- Understand the idea behind denoising
- Describe how we can walk back through "time" to get a new image from noise
- Understand those gradient graphs we have been looking at all week.
- Explain how the amount of noise added affects curvature of graphs
* Explain what happens when you are trying to remove noise from very noisy image against from not really noisy image.
* Provide the formula of loss function that is optimized by the denoiser.
    – `MSE = X(denoised img − clean img)^2`
* Describe why we need to re-evalutate the noise reduction after some amount of noise is reduced.

    – If we didn’t → we add something into the image what is not suposed to be in the result. [I don't believe this is correct. Rather, you would only get the mean image of ALL images rather than a single good image.]
	
    – If we do it too often → it takes too much time to compute. [True]



Challenging topics:

* Explain the actual calculations in a denoising function
* 2. The denoising blog post explains preconditioning withing the denoise() algorithm, how does this differ from the initial denoise() algorithm?
* 1) Fully explain how the denoise function works [What steps remain that you couldn't explain?]
* 2) Implement optimized tuning parameters for denoising. [Of course!]
* Explain the actual calculations in a denoising function
* a. Write pseudocode surrounding ODEs
* Implement denoise algorithm from scratch.
* Do any with math for diffusion.
* Determine whether this description is valid: "The function in Figure 5 uses randomness with some extra forces to ensure that the noise won’t output more noise."
* Explain why, in Figure 5, they end up with dog and in Fig. 6 with cat – if you go to source 1, shouldn't it be the same animal each time?
* 1. Describe how the optimal noise schedule is determined
* b. Explain how to create a noise schedule.
* b. Write pseudocode for noise schedules.
