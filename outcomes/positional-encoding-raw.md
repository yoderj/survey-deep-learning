Raw results
-----------

**O1. Write three or more outcomes for things you are comfortable doing after this further reflection on the paper.**

 * Explain the computation of each linear layer
 * Explain why sin/cos was used for positional encoding
 * Write the equation to represent $PE_{pos+k}$ as a linear equation.
 * Explain why $PE_{pos+k}$ can be represented as a linear equation of $PE_{pos}$ with offset k.
 * Identify the shapes of matrices that are used in this process.
 * - Explain how the offset, $k$, is used in the embedding calculations
 * - Summarize how sine and cosine, nonlinear equations, are able to be mapped using a linear layer
 * - Write psuedocode that maps $PE_{pos+k}$ to $PE_{pos}$
 * Knowing what this equation is showing A @ x = B 
 * Understanding the numpy lstsq 
 * Following Attention
 * 1: Describe positional embeddings as well as describe how they are calculated
 * 2: Explain linear mapping of positional embeddings
 * 3: Write basic psuedo code for positional embeddings
 * - Explain why positional encodings are important to the Transformer architecture.
 * - Explain how sine and cosine as nonlinear functions create a linear function when combined.
 * - Write the equation for the linear transformation of $PE_{pos}$ into $PE_{pos+k}$
 * 1. Explain how for any fixed offset $k$, $PE_{pos+k}$ can be represented as a linear function of $PE_{pos}$."  
 * 2. Given A * x = b explain what the x represents in the equation
 * 3. Explain why linearity is important in Attention 

**O2. Write one to three outcomes for things you think are important to be able to do after the reading, but which you cannot do yet.**

 * 1: implement the transformer from scratch
 * 2: describe how you could optimize the model
 * Explain how relative positional embeddings work mathematically.
 * Write Psudocode for positional attention.
 * Explain what is happening between 0 and 10 in the last colorbar
 * Explain why PE is important to attention. I specifically did not understand this part of the assignment.
 * - Explain how the positional attention matrix shifts its inputs
 * Which way does the matrix shift? Left or Right  
 * What this sentence means "We chose this function because we hypothesized it would allow the model to easily learn to attend by relative positions, since for any fixed offset $k$, $PE_{pos+k}$ can be represented as a linear function of $PE_{pos}$"   
