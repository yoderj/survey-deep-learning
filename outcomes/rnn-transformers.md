One of the major disadvangates of transformers is their computation cost: We must re-analyze much of the previously-produced text on every time-step to produce a good next token.

There have been several works that have attempted to work around this.  It's not clear that any of these techniques are successful yet.

* Cast Transformer as an RNN, but mostly eliminate the nonlinearity of Softmax \[[2006.16236](https://arxiv.org/pdf/2006.16236.pdf)\]
* Cast Transformer as an architecture that can be learned in parallel, but computed recursively.  Again, approximations might not be good as the softmax. \[[2307.08621](https://arxiv.org/pdf/2307.08621.pdf)\]

Predict multiple tokens at once:

* Predict multiple tokens in a single pass. Claims 30% speed-up. [2311.13581](https://arxiv.org/pdf/2311.13581.pdf)

