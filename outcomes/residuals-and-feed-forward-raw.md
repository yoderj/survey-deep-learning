"I did not use ChatGPT on this assignment, although I did use Microsoft word’s autocomplete feature which according to lecture is pretty much all that transformers are." -- :-O LOL


# Achieved outcomes

* Describe what the ReLU operation is
* Describe why using ReLU as opposed to other methods is faster for training
* Describe what a residual is.
* Define ReLU function and write at least two reasons why it might be better than f (x) = tanh(x).
* (b) Write an expression which satisfies the residual layer.
* (c) Draw how ”Feed Forward” sub-layer might look in transformer from the "attention is all you need" paper.
* 1: Define the ReLU layer and why it is used and the problem is solves
* 2: Explain what degradation is and how it is a problem for modals
* 3: Explain the purpose of the multi-head self-attention layer
* 1. Graph the ReLU function.
* 2. Explain the purpose of shortcut connections.
* 3. Describe the benefits of using a non-linear operation.
* Describe what is degradation when it applies to deep learning and neural networks.
* - Explain the purpose of the ReLU Operation.
* - Explain why residual layers make learning easier.
* Write pseudocode for the ReLU operation.
* • Explain the purpose of skip connections.
* • Explain why ReLU is used and what problems it solves
* Write the mathematical formula for the ReLU activation function.
* Explain the degredation problem and how the ReLU addresses it.
* Sketch a diagram showing the flow of data through a residual connection.
* Explain the problem with deeper neural networks.
* Explain how ReLU works and give an example.
* Explain how residual layers and residual connections together solve the problem with deeper neural networks.

# Important yet not yet achieved outcomes

## Outcomes Dr. Yoder believes are core to the topic
* 1: fully understand the Position-wise Feed-Forward network
* Write pseudocode for ReLU Operation. ->   y = max(0,x)   OR   y = (x>0) * x
* Write pseudocode for Feed Forward sub layer.
* Explain how deep residual learning optimizes model training to train deeper networks.
 
## Uncategorized outcomes
* Define what the vanishing gradient problem is [As far as I know, vanishing gradients are no longer a concern once deep networks use proper skip connections. This was more a problem with the simpler recurrent networks, where the LSTM is essentially an RNN with skip-connections built in.  [Though you won't find anyone online being this clear about that.](https://duckduckgo.com/?q=User+Can+an+LSTM+be+considered%2C+at+a+high+level%2C+to+be+an+RNN+incorporating+explicit+%22skip-connections%22+across+time+or+sequence%3F&t=newext&atb=v378-1&ia=web) (no hits). [Well, now you can.](https://chat.openai.com/share/031213db-6185-415e-82bf-98166158c6ce) ]. [Or is that even right?](https://chat.openai.com/share/d5798fe3-4b2e-462d-a54e-599c8c93e898)
  * *Describe* the core differenes between an LSTM and the RNNs that preceded it. [Blog by a Google employee](https://colah.github.io/posts/2015-08-Understanding-LSTMs/)
  * *Explain* the strenghs of LSTMs relative to Transformers (and of course, the weaknesses as well)
* Describe why residuals are superior to not using residuals.
* Elaborate on why skipping some layers would be better than skipping others (Ex, why would you want to skip 1 layer with a residual connection as opposed to 2-3) [My guess, unsupported by any experiment, is that you want to skip groups of layers that perform some small nonlinear adjustment to the signal passing through the skip connection.  My guess is that it's not too important exactly how many layers you skip. This would be a good topic for an undergraduate research study.  Experiments could strengthen our intuition of this topic.]
* Explain what [layer] normalization in different sub-layers of transformer mean. [1803.08494]
* Identify what problems ReLU would be best suited for and when it is not suited for the problem at hand. [I'm not interested in this subject. I'd be happy to be trained to be interested by the students...]
* Describe how ReLU and tanh introduce nonlinearlity to the network. [This is an outcome from CSC 4611 -- the Deep Learning required course. I will only introduce it here if the majority of students request we add it or this course needs it for some higher-level topic.  Softmax is a nonlinear operation whose role is more critical to this course, and for which the motivation is clear.]
* Explain why overfitting is not an influence/problem compared to depth for deep neural networks. [This is also a topic from Deep Learning. Ditto the previous outcome on when we will discuss it.]
