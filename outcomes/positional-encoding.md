## Required outcomes

Credit goes to the students of the Spring 2024 offering of this elective for identifying a rich, balanced set of outcomes from the reading.

### More difficult outcomes
This section contains outcomes the class was NOT able to do after the reading -- higher priority for in-class discussion.

 * *Define* *causal* and *non-causal* attention in the context of a generative decoder-only transformer
 * Explain how the positional attention matrix shifts its inputs
 * Explain why positional embedding is important to attention
 * What this sentence means "We chose this function because we hypothesized it would allow the model to easily learn to attend by relative positions, since for any fixed offset $k$, $PE_{pos+k}$ can be represented as a linear function of $PE_{pos}$"   

### Easier outcomes
This section contains outcomes that were easier for students and require less in-class discussion.

 * Explain why positional encodings are important to the Transformer architecture and to attention
 * Explain why linearity is important in Attention 
 * Explain how for any fixed offset $k$, $PE_{pos+k}$ can be represented as a linear function of $PE_{pos}$
 * Identify the shapes of matrices that are used in this process
 * Describe positional embeddings 
 * Describe how positional embeddings are calculated
 * Interpret the numpy lstsq documentation
 * **Discuss** the sine and cos equations used to compute positional embeddings
   * Explain how the offset, $k$, is used in the positional encoding calculations
 * Write pseudocode that maps $PE_{pos+k}$ to $PE_{pos}$ (as a linear equation without specifying values within the matrix)

## Optional outcomes
Most of these were also written by a student. Several are related to the optional part of the reading.

 * Explain how sine and cosine as nonlinear functions create a linear function when combined.
 * Write the equation for the linear transformation of $PE_{pos}$ into $PE_{pos+k}$
   * Write psuedocode that maps $PE_{pos+k}$ to $PE_{pos}$ (as a linear equation with a formula for each value in the matrix)
   * Explain how the offset, $k$, is used in the k-shifting linear mapping calculations
 * Given A * x = b explain what the x represents in the equation
 * Summarize how sine and cosine, nonlinear equations, are able to be mapped using a linear layer
 * Explain why sin/cos was used for positional encoding

## Optional outcomes
(These were also written by students, but under the category of things they couldn't do yet.)

 * Implement the transformer from scratch
 * Describe how you could optimize the model
 * Write Psudocode for positional attention.
 * Explain how relative positional embeddings work mathematically.

# (Optional) Further reading on Positional Encoding

* *Explain* how temperature can be a learnable parameter (or rather, the output of a learned subnetwork) [[2309.02772](https://arxiv.org/pdf/2309.02772.pdf)]
* *Describe* the differences between learned and absolute positional encodings [[2010.04903](https://arxiv.org/pdf/2010.04903.pdf)]
* *Describe* what learned position encodings/embeddings learn [[2010.04903](https://arxiv.org/pdf/2010.04903.pdf)]
* *Describe the advantages* of absolute positional encoding (such as the sinusoid functions used in [2309.02772]) over learned embeddings when evaluating on text larger than used for training (length generalization) [[2305.19466a](http://export.arxiv.org/pdf/2305.19466)].
* *Describe* when addition (vs concatenation) of positional embeddings to word embeddings is appropriate.  There aren't good references for this yet. There's some starter discussion [on stack overflow](https://datascience.stackexchange.com/questions/55901/in-a-transformer-model-why-does-one-sum-positional-encoding-to-the-embedding-raa)
