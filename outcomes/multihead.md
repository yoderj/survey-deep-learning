# Achieved Outcomes

Again -- credit goes to the students of the Spring 2024 offering of this elective for identifying a rich, balanced set of outcomes from the reading.

* Explain the "idea" of multiple heads -- (beyond motivation and structure?)
* Explain why all transformers use multiple heads
  * Explain why the output of the various heads should be concatenated
* Sketch the Multi-Head Attention layer
* Write pseudocode/formulas for computing single and multi-head attention
  * Write the formulae for each step
  * Specify along which dimension the concatenation is performed when combining heads
* Enumerate the dimensions of each block in Multi-Head Attention based on given number of heads h
  * Explain how differences in dimensionality will affect Q, K, and V values and how model and head size will change
  * Describe how the compute required for the transformer was able to remain relatively the same even though seven additional attention heads were added
  * Explain why the dimensionality of the inputs to the multi-head attention is reduced rather than maintaining their original size
* Explain how each head works independently
  * Explain the importance of the input linear layers
  * Explain why each head results in a different output
* Describe how the outputs of different heads are connected into a single tensor after they leave Scaled Dot-Product Attention
* Explain why there are linear layers after the multi-heads
  * See instructor "aha" below
* Compute the output of a multi-head attention layer

# (Optional) Advanced

* Contrast concatenation and addition as alternative for combining the results of the heads
* Compare the compute necessary for the attention layer with the compute necessary for the feed-forward layer
* Implement multihead attention in code by following the examples from d2l.ai

# Not-yet-complete Outcomes

## Overlap with "achieved outcomes"

Some of these outcomes are not overlap, but rather things discussed in class

* Explain that all transformers use multiple heads
* Describe the implementation of the Multiheaded Attention
  * Write out an example of an attention layer, similar to the pytorch we went through in class
  * Fully compute the attention equations
* Explain how each head works independently
  * Explain how the heads are kept from learning the same things
  * Explain why each of the heads learn something different rather than all learning the same feature
* Explain why there is no activation function applied to multiheaded attention
* Describe the shape of the head_i for each of the individual heads
* Explain the purpose of $W^O$ matrix after concatenation
* Learn to interpret dimensionality easier (discussed in the context of the feed-forward network)

## Non-overlap with "Achieved outcomes"
* Explain how weights are distributed (These are learned during training)
* Better understand the work behind the attention layer (prior work?)
* Write optimization methods for the attention or softmax calculations would be important, such as finding 1/sqrt(d(k)) or like optimizations
* Describe how is this possible: ”Due to the reduced dimension of each head, the total computational cost is similar to that of single-head attention with full dimensionality.”


# Why Linear Embeddings are needed
Instructor remarks:

I finally get it!  Something in your outcomes made this click for me!

When a network uses a residual structure, the meaning of each portion of the embedding is carried forward from the input of the section to the output, with only minor adjustments to the embedding made by each layer

The linear layer that follows the softmax embedding is necessary to allow the head embeddings to map onto the different parts of the full embedding related to their work.  For example, the linear embedding might simply route the output of a grammar head that determines a part of speach to a part-of-speech region of the embedding.  (In practice, it will be hard to see this, because "part of speach" is likely to be a direction within the embedded space rather than a few adjacent elements of the embedding.)

As many of you said, it allows the outputs of the different heads to be "blended" into a single head.  Yet it is a bit more than blending.  It is a redirecting into an existing latent space
