One student shares [this Obsidian .canvas file](wordpiece.canvas), which is [linked in the first offering's Canvas course](https://msoe.instructure.com/courses/17792/pages/wordpiece-models-dot-png-note?module_item_id=697931) as a png, including embedded figures, which are not included here.

After doing this reading, I can:
--------------------------------
* 1: Explain what a Wordpiece is and how it helps manage large vocab datasets
* 2: Explain what Byte pair Encoding is and how it uses pairing and frequency to connect symbols together
* 3: Explain how unknown words in the initial datasets can be handle with wordpieces and byte pair encoding.
* Explain how the wordpiece model splits up words into multiple wordpieces.
* Write the pseudocode for a wordpiece model, it should take in two parameters, n, the number of merges to perform, and lang, a list containing all of the characters in the source language.
* List a few of the issues that are common in NMT that must be accounted for when designing a model
* Be able to explain the two categories of approaches when translating out of vocabulary (OOV) words.
* Describing the process of how wordpieces are formed.
* Explain the encoding in second part of the first paper, where they use < B >, < M > and < E > to encode whether the character is in the beginning, middle or at the end of the word.
* Explain the basic idea of byte pair encoding algorithm.
* Find the BPE of a set of words
* Explain why BPE is needed and how it is used
* Explain the difference between the two
* Understanding what an NMT is for
* Understanding the difficulties that rare words make for translation tasks
* Understanding the uses of breaking words into smaller pieces
* Describe the approach taken in what it is doing with pairing words into wordpieces.
* Describing what byte encoding is
* Explaining what word piece is
* Explain why wordpieces are used
* Explain how the word piece model functions and what word pieces are.
* Calculate the size of a BPE vocabulary post translation.
* Understanding why consistency increases with BPE/Wordpiece by translating both languages into one, and then back as it learns joint BPE encodings.


I would like to but cannot yet:
-------------------------------
* 1: Write pseudo code for a wordpiece (If I though about it long enough I could throw something together)
* 2: Fully explain and understand what is happening in the BPE algorithm with the example pseudo code.
* 3: Explain the full scope of how byte pair encoding works and if there are more efficient ways to even make it better.
* 1. Write pseudocode of a function that creates the wordpiece vocabulary.
* 2. Explain the performance and memory boost by using wordpiece in transformers.
* 3. Identify the strengths and weaknesses of other segmentation approaches.
* Explain how residuals address the exploding and vanishing gradient problems.
* Explain the purpose for bidirectionality in the encoder, what problem does this address?
* Show using a drawing how data parallelization is performed.
* 1. Be able to explain other possible reasons why a word would be broken into two wordpieces.
* 2. Be able to explain the steps for Byte Pair Encoding (BPE).
* 3. Be able to explain the 2 methods of applying BPE
* Recognize what words will require more than one wordpiece.
* Recognize when WordPiece will be beneficial to the overall project.
* Who to implement WordPiece into projects in the future. (???)
* Describe what the [BLEU metric](https://en.wikipedia.org/wiki/BLEU) (bilingual evaluation understudy) stands for, but it might be interesting to explore it.
* Describe the interactions between WordPiece and BLEU [https://en.wikipedia.org/wiki/BLEU, "BLEU is infamously dependent on the tokenization technique,", citing "BLEU: A Misunderstood Metric from Another Age"(https://towardsdatascience.com/bleu-a-misunderstood-metric-from-another-age-d434e18f1b37), a paywalled article]
* Explain how the model connects wordpieces to real concepts. Where I see the problem is that how they used it for translation, because the wordpieces are some pieces of not full words, so they cannot be translated. For example, if I get the wordpiece ud I don’t know, which word it belongs to.
* Explain the connection between these papers. In first paper they purposed that the special symbol of space is everytime in the beginning of the wordpiece. While in the second paper after they use the byte pair encoding, it outputs wordpieces with space even in the beginning and in the ending of the piece.
* *Explain* how a network can interpret ambiguous words and word-fragments which have no meanings until combined with their neighbors.
* Write out some examples of wordpiece
* Explain why wordpiece is needed and how it is used practically
* Point out where in the transformer with would be taking place
* Compare training metrics / Evaluation metrics for translation models
* Explain all of section 5 of the GNMT paper ("Training Criteria" -- dense mathematics presented with little intuition)
* Explain what a residual connection is
* Explain how separating words into wordpieces enables easier translation of words specifically in regards to going from alphabetical to logographic because it seems like capturing the meaning is more important than the word’s structure.
* Explain why this approach works (because the BLEU (bilingual evaluation understudy) scores say it does)
* Explain what Out of Vocabulary words refer to. Are they just colloquial terms/words, or is it that more unique and untranslatable words that cannot be directly translated?
* Decide whether BPE (Byte Pair Encoding) a type of training model to allow the creation of wordpieces based on words? Or is the association between the two different from that?
* Explain why wordpieces perform better than (or rather, how it supports) the transformer model we evaluated last paper
* Explain in more details what efficiencies that the authors use to speed up the algorithm
* Actually code a wordpiece model for production
* Explain why it is important to remove the need for a shortlist to maximize efficiency.
* Create a wordpiece algorithm that correctly assigns # of word pieces to words, i.e. I would not think to assign 2 word pieces to jet.
* Explaining how the word piece model can guarantee to generate a deterministic segmentation for any possible sequence of characters.
