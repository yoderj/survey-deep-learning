In addition to being required for Transformers that are otherwise ["permutation](https://datascience.stackexchange.com/a/16084/53330) [equivariant"](https://aclanthology.org/2021.emnlp-main.236.pdf), positional encodings are also found in mouse brains (and presumably human brains) in grid cells.

These outcomes explore that topic.

* Explain the function of grid cells [[May-Britt Moser - How Do We Find Our Way? Grid Cells in the Brain](https://kids.frontiersin.org/articles/10.3389/frym.2021.678725)]
* Explain the function of place cells [[John O'Keefe - Place Cells: The Brain Cells That Help us Navigate the World](https://kids.frontiersin.org/articles/10.3389/frym.2023.1022498)]
* Describe goals for future work in the neurobiology of grid and place cells [[Edvard Moser and Noa Segev - Can Grid Cells Help Us Understand The Brain?](https://kids.frontiersin.org/articles/10.3389/frym.2023.1151734)]
* Describe similarities and differences between grid cells and positional encoding.
* Describe differences and similarities between place cells and softmax classification output cells.
* Explain why neural networks may *not* yet be good models of how grid cells learn [No Free Lunch from Deep Learning in Neuroscience](https://openreview.net/pdf?id=mxi1xKzNFrb)
* Describe the differences between learned embeddings and sinusoidal positional encodings [What Do Position Embeddings Learn?](https://aclanthology.org/2020.emnlp-main.555.pdf)
* Note the similarity between non-sinusoidal learned embeddings and non-grid cell learned embeddings.

