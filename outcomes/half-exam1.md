All novel outcomes for this exam have been copied into the topic-specific outcomes files.

*This will be an in-class on-paper test (like all tests and quizzes)*

*You may bring one note-sheet, prepared by yourself. Both sides. You
will submit your notesheet with the exam.*

## *Transformer Architecture*

-   *Sketch the general architecture of a transformer network*

## *Attention (Roughly one page)*

-   *Perform* matrix multiplication, given the input matrices.*\
    *

-   *Discuss* the dimensions of variables in matrix multiplication

-   *Represent* a linear layer with matrix multiplication (full text
    input (many tokens))
    -   ***Write* an equation using matrix multiplication for a
        fully-connected layer given row- or column- ordered data and the
        dimensions of the matrices**
    -   **Given a matrix multiplication representing a layer, *discuss*
        the dimensions of the matrices within that equation**
    -   ***Determine whether* a matrix multiplication represents
        independent operations on rows or columns (as in a linear layer
        applied to each token), inner products of rows or columns, or
        convex combinations of rows or columns.**

-   *Describe* how one-hot encoding with a fully-connected layer and
    direct token embeddings are theoretically related. (lecture,
    1706.03762)

-   *Describe* the general structure of a Transformer.

    -   *Describe the general structure of a Transformer.*
    -   *Illustrate* Where embedding, the attention operation, linear
        layers, and the softmax layer are found within the Transformer.
        (1706.03762)
    -   *Describe* how each operation interacts with those before or
        after it. (1706.03762)
    -   *Describe*, at a very high level, how textual information flows
        through and is transformed by the Transformer architecture.
        (This explanation will be able to have more and more detail as
        you fit in outcomes that come later.) (1706.03762)

-   *Write pseudocode* for the input and output layers of a Transformer

    -   *Write pseudocode for the input and output layers of a
        Transformer*
        -   ***Write pseudocode for embedding lookup?  row-ordered:  
            all_embeddings\[index_word, :\]\
            ***
        -   ***Write pseudocode or describe algorithm for selecting a
            word given softmax output?***
    -   *Write pseudcode* for the softmax operation. (lecture)
    -   **Describe the purpose of the softmax layer.**
    -   **Describe the input range legal for a softmax layer.**
    -   **Describe the output range a softmax layer produces.**
    -   **Describe what is true of the sum of the outputs of a softmax
        layer.**
    -   *Write pseudocode* for a linear layer with bias. (lecture)

-   *Describe* embedding and decoding of embedding

    -   *Describe embedding and decoding of embedding*
        -   ***Describe embedding lookup***
        -   ***Describe how words are selected at output of network***
        -   ***Describe why a linear dimensionality-increasing layer is
            needed at the output***
    -   *Describe* how a Transformer selects the next word of its output
        within the final layer.

-   *Write an executable-clarity definition* (e.g., write pseudocode) of
    the attention operation

    -   *Write an executable-clarity definition (e.g., write pseudocode)
        of the attention operation*
        -   ***Write the equation for the softmax layer?***
        -   ***Write the equation for attention, given softmax? (Q,
            K\^T, V, all together?)   softmax(Q K\^T) V\
            ***
    -   *Describe* how the softmax operation should be normalized, given
        row-ordered or column-ordered data

-   *Describe* the operation of the attention layer.

    -   [*(STRIKE) Describe the operation of the attention
        layer.*]{style="text-decoration: underline;"}
        -   [***Describe key
            matching?***]{style="text-decoration: underline;"}
        -   [***Describe why separate embeddings are needed for each
            input?***]{style="text-decoration: underline;"}
    -   *Describe* how the softmax operation should be normalized, given
        row-ordered or column-ordered data

# WordPiece

-   *Give examples* of problems with a fixed-vocabulary model that a
    WordPiece model can solve.
-   *Describe* the steps of the algorithm for constructing a WordPiece
    encoder
    -   *Describe* the initial vocabulary used by the WordPiece model
        before tokens start to be merged.
    -   *Describe* the criterion used to merge two WordPiece units into
        one.
-   *Give an example* of what the wordpieces might look like for a short
    input.

## Beyond Scope

-   (NOT ON EXAM) Explain how attention allows for a context to be
    formed around a given token
-   (NOT ON EXAM) Describe the problem with ReLU that Swish addresses
    \-- the \"dying ReLU problem\"
-   (NOT ON EXAM) Describe why nonlinear activation functions are
    needed.
-   (NOT ON EXAM) Describe what is degradation when it applies to deep
    learning and neural networks: Why adding layers HURTS performance.
    "When deeper networks are able to start converging, a degradation
    problem has been exposed: with the network depth increasing,
    accuracy gets saturated (which might be unsurprising) and then
    degrades rapidly. Unexpectedly, such degradation is not caused by
    overfitting, and adding more layers to a suitably deep model leads
    to higher training error, as reported in \[11, 42\] and thoroughly
    verified by our experiments. Fig. 1 shows a typical example."  This
    is what motivated "identity" layers -- "degredation" is a good
    keyword linking to discussion in this paper that several students
    caught and said they could explain that I skipped over until
    randomly seeking outcomes for this exam.

## Student outcomes considered

Some of these may be on the exam and others not -- please see the
outcomes above instead.

This is a random subset of student outcomes I reviewed while trying to
prepare the outcomes for this exam.

* following a deep learning explanation/following the explanation and
architecture of a transformer model.
* Explain the purpose of the softmax layer.
* Explain why residual layers make learning easier.
* Describe what is degradation when it applies to deep learning and neural
* networks.
* Explain why wordpieces are used
* Explain how attention allows for a context to be formed around a given
* token
* Explaining what word piece is
* 2: Explain what degradation is and how it is a problem for modals
* Calculate the size of a BPE vocabulary post translation.
* Give an example of a use use of dropout the layer.
* Understanding why consistency increases with BPE/Wordpiece by
* translating both languages into one, and then back as it learns joint
* BPE encodings.
* Explain the degredation problem and how the ReLU addresses it.
* 1.) Explain the purpose of why certain parts of the transformer model
* are present
* Describing what byte encoding is
* 3: Explain how unknown words in the initial datasets can be handle with
* wordpieces and byte pair encoding.
* 3: Explain the purpose of the multi-head self-attention layer

## References

Wherever possible, this document uses references to arxiv papers.  The
URL to an arxiv paper can easily be extracted from the number. For
example, the URL to the arxiv article arxiv:1508.07909 is
[https://arxiv.org/pdf/1508.07909.pdf](https://arxiv.org/pdf/1508.07909.pdf){target="_blank"
rel="noopener"}

 
