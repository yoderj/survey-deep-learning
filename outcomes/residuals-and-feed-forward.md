# Achieved outcomes

## ReLU
* *Write* pseudocode for the ReLU activation function. (e.g., y = max(0,x)  OR   y = (x>0) * x, etc.)
* *Describe* where ReLU is found within the Tranformer "Attention is All You Need"
* Describe what the ReLU operation is
* Define ReLU function
* Graph the ReLU function

## Residual Connections
* *Describe* what a residual is
* *Write* pseudocode for the Residual connection
* *Sketch* the feed-forward subnetwork used by a Transformer
* *Describe* how residuals improve training of Transformers
* Sketch a diagram showing the flow of data through a residual connection
* Explain the purpose of shortcut (also called residual or skip) connections
* Explain why residual layers make learning easier
* Write pseudocode for (e.g. an expression describing how to compute) the residual layer

## Feed-forward sub-network
* Sketch how the ”Feed Forward” sub-layer might look in the transformer from the "attention is all you need" paper
* Describe where dimensionality increase and reduction occurs in the position-wise feed-forward network used by the transformer.
* Write pseudocode for Feed Forward sub layer.
* Explain how deep residual learning optimizes model training to train deeper networks.

## (optional) Advanced outcomes
* Define how ReLU operates on a vector
* Compute the output of ReLU for a given vector
* Describe why a nonlinear activation function like ReLU is required in a deep network
* Describe why using ReLU as opposed to other methods is faster for training
* Write at least two reasons ReLU might be better than f(x) = tanh(x).
* Explain the problem with deeper neural networks
  * Explain why very deep networks see lower (degraded) performance compared to shorter networks
* Explain how residual layers and residual connections together solve the problem with deeper neural networks
* Explain why deep networks experience a degredation in performance not seen in shallower networks. [Here's my shot at answering it.](https://datascience.stackexchange.com/a/126976/53330).  Perhaps you can explain it better -- lots of upvotes on the question so far, not so much on the answers.

For exploration of topics beyond scope for this elective, yet of interest to other students, see the [raw student outcomes](residual-and-feed-forward-raw.md)
