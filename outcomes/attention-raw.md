These are all the outcomes submitted by students in the first reading, edited slightly to fit closer to an outcome format.

Things you can do
=================
* a. explain the concept of transformers and the concept of their architecture.
* b. explain what embeddings are and how they effect the overall transformer, as well as how they are used.
* c. explain why transformer model uses a scaled dot-product modal.
* Explain how embeddings are developed, and why they are used
* Explain how attention allows for a context to be formed around a given token
* Explain the purpose of a softmax layer
  * A definition of the goal of softmax: The softmax layer is used to find the highest values without making the resulting matrix binary, this means that it is possible for multiple similarly probable values to be sampled
* a. Represent a non-numerical input using one-hot encoding and embeddings.
*  compute an example of how matrix multiplication is used to generate the output
* layer.
*  explain what an embedding is in relation to Transformers.
*  explain how One Hot Input works and affects the outputs.
* 1.) Explain the purpose of why certain parts of the transformer model are present
* 2.) Discuss the vocabulary involved in the transformer model
* 3.) Explain why the transformer model is able to outperform CNNs when it comes to LLMs
*  - think of uses for attention / what tasks Transformer models are good for.
*  - use embeddings in models and how the benefit the models learning process
* Following how scaled-dot product attention works, and how it involves the use of matrix multiplication to produce attention values to determine attended information.
* following a deep learning explanation/following the explanation and architecture of a transformer model.
* Understand the layers of the transformer.
* Understand the inputs and outputs of the encoder and decoder.
* Understand the training process of the model described in the paper.
* Explain the purpose of the softmax layer.
* Give an example of a use use of dropout the layer.
* Imagine how networks perform mathematical operation of matrix multiplication.
* Set up the equation for computing the Scaled Dot-Product Attention


Thing you don't yet know how to do
=====
* a. Actually implementing the Transformer model in code, which I don’t think I can do after reading this but would be important for understanding how it works in code
* c. How to analyze and optimize the performance of the Transformer and its tasks. Which means things like translations and how it summarizes text in other languages
* Explain how multi-head attention is similar to and different from self-attention
* Explain how the positional encoding are implemented and applied to the embedded data
* Explain the purpose of the residual connections found in the architecture
* a. Explain why sine and cosine functions are used in the positional embeddings.
* b. Implement the encoder/decoder transformer architecture from scratch.
* c. Explain why scaling is important in the scaled dot product attention function
*  I think being able to apply a (small-scale) version of the concepts to actual code would be important.
* o Theory and actual applying theories, especially with AI, ML, etc.
*  Use the Attention Layer formula to do that sort of math
*  Review of Linear Algebra terminology as some words relating to it aren’t words I recognize right now.
* 1.) code a transformer model
* 2.) describe how transformers apply to areas outside of word input
* 3.) describe how the positional encoding operates.
*  - Implement Scaled Dot-Product Attention
*  - Understanding the benefits of Multi-Head Attention
*  - Being able to implement an attention based transformer for a given task
* create and implement a basic version of the transformer model with self-attention.
* understand and implement parameter/hyperparameter differences in a model, such as varied N, dmodel , h, etc. values as well as developing positional encoding functions such as the ones developed in the paper using sin and cos of (pos/10,0002i/dmodel ) respectively.
* Find cases for and apply attention-based models to non-machine translation tasks as was mentioned in the paper, as finding more uses for the technology allows a better understanding of and implementation of it.
* Apply the transformer to future projects.
* Understand the importance of positional encoding for this project.
* Understand the transformer architecture. I knew about fully connected neural networks, convolutional networks a little bit about existence of recurrent networks, but that was it. Terms like transformer, encoder, decoder are very new for me and I am completely confused by them.
* Positional encoding is another weird thing to me. Here I fight with that sine and cosine are periodical functions returning a number between -1 and 1. After calling them on a positions divided by something, it returns a number, but what period is it in, do we even care about it?
* Explain what attention is
* One outcome would be further understanding Multi-Head Attention
* The second outcome would be further understanding Position-wise Feed-Forward Networks
* The third outcome would be further understanding what attention is


## BEYOND CURRENT PAPER
b. How to extend the Transformer architecture to other tasks like image recognition.

