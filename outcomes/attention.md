References
----------
Wherever possible, this document uses references to arxiv papers.  The URL to an arxiv paper can easily be extracted from the number. For example, the URL to the arxiv article arxiv:1508.07909 is https://arxiv.org/pdf/1508.07909.pdf

Linear Algebra
---
* *Interpret* matrix multiplication as performing weighted (or linear) combinations of the rows of the right matrix (or columns of the left matrix)
* *Interpret* matrix multiplication as performing independent transformations on the rows of the left matrix (or columns of the right matrix)
* *Interpret* matrix multiplication as computing dot products (inner products) of all of the rows of the left matrix with all columns of the right matrix 
* (for later) *Interpret* matrix multiplication as computing the sum of the outer products of all columns of the left matrix with all rows of the right matrix


Preliminaries -- Week 1 Day 1
----
* Day 1: 
  * The goal of softmax: the softmax layer is used to find the highest valued inputs for a sample without making the corresponding resulting outputs binary (with just a single 1-hot encoded value and the rest zeros).  For transformers, this means that instead of selecting the single most probable token, it is possible for multiple similarly probable tokens to be sampled in different random text generations.
  * Define the softmax operation
  * Review matrix multiplication
  * Review the matrix transpose operation
  * Review that data can be stored row-wise or column-wise
  * Discuss one-hot encoding, as a theoretical way of embedding.
  * Special tokens for embedding for classification


Attention -- Weeks 1 and 2
----
* *Describe* the overall architecture and major features of the first Transforer, "Attention is All you need." (1706.03762, [Youtube Summary](https://www.youtube.com/watch?v=TQQlZhbC5ps))

* *Describe* how one-hot encoding with a fully-connected layer and direct token embeddings are theoretically related. (lecture, 1706.03762)

* *Describe* the general structure of a Transformer. (1706.03762, [Youtube Summary](https://www.youtube.com/watch?v=TQQlZhbC5ps))
  * *Illustrate* Where embedding, the attention operation, linear layers, and the softmax layer are found within the Transformer. (1706.03762,[Youtube Summary](https://www.youtube.com/watch?v=TQQlZhbC5ps))
  * *Describe* how each operation interacts with those before or after it. (1706.03762,[Youtube Summary](https://www.youtube.com/watch?v=TQQlZhbC5ps))
  * *Describe*, at a very high level, how textual information flows through and is transformed by the Transformer architecture. (This explanation will be able to have more and more detail as you fit in outcomes that come later.) (1706.03762,[Youtube Summary](https://www.youtube.com/watch?v=TQQlZhbC5ps))

* *Write pseudocode* for the input and output layers of a Transformer
  * *Write pseudcode* for the softmax operation. (lecture)
  * *Write pseudocode* for a linear layer with bias. (lecture)

* *Describe* embedding and decoding of embedding
  * *Illustrate* the concept of embedding words into a space ([Youtube Summary](https://www.youtube.com/watch?v=TQQlZhbC5ps),[Youtube Apple Disambiguation](https://www.youtube.com/watch?v=UPtG_38Oq8o)) 
  * *Describe* how a Transformer selects the next word of its output within the final layer.
  * *Describe* how a Transformer selects the next word, by giving earlier words as context ([Youtube Apple Disambiguation](https://www.youtube.com/watch?v=UPtG_38Oq8o))

* *Write an executable-clarity definition* (e.g., write pseudocode) of the attention operation 
  * *Describe* how the softmax operation should be normalized, given row-ordered or column-ordered data

* *Describe* the operation of the attention layer.
  * *Describe* how the softmax operation should be normalized, given row-ordered or column-ordered data


## *Transformer Architecture* -- On Half Exam 1

-   *Sketch the general architecture of a transformer network*

## *Attention (Roughly one page)* -- On Half Exam 1

-   *Perform* matrix multiplication, given the input matrices.*\
    *

-   *Discuss* the dimensions of variables in matrix multiplication

-   *Represent* a linear layer with matrix multiplication (full text
    input (many tokens))
    -   ***Write* an equation using matrix multiplication for a
        fully-connected layer given row- or column- ordered data and the
        dimensions of the matrices**
    -   **Given a matrix multiplication representing a layer, *discuss*
        the dimensions of the matrices within that equation**
    -   ***Determine whether* a matrix multiplication represents
        independent operations on rows or columns (as in a linear layer
        applied to each token), inner products of rows or columns, or
        convex combinations of rows or columns.**

-   *Describe* how one-hot encoding with a fully-connected layer and
    direct token embeddings are theoretically related. (lecture,
    1706.03762)

-   *Describe* the general structure of a Transformer.

    -   *Describe the general structure of a Transformer.*
    -   *Illustrate* Where embedding, the attention operation, linear
        layers, and the softmax layer are found within the Transformer.
        (1706.03762)
    -   *Describe* how each operation interacts with those before or
        after it. (1706.03762)
    -   *Describe*, at a very high level, how textual information flows
        through and is transformed by the Transformer architecture.
        (This explanation will be able to have more and more detail as
        you fit in outcomes that come later.) (1706.03762)

-   *Write pseudocode* for the input and output layers of a Transformer

    -   *Write pseudocode for the input and output layers of a
        Transformer*
        -   ***Write pseudocode for embedding lookup?  row-ordered:  
            all_embeddings\[index_word, :\]\
            ***
        -   ***Write pseudocode or describe algorithm for selecting a
            word given softmax output?***
    -   *Write pseudcode* for the softmax operation. (lecture)
    -   **Describe the purpose of the softmax layer.**
    -   **Describe the input range legal for a softmax layer.**
    -   **Describe the output range a softmax layer produces.**
    -   **Describe what is true of the sum of the outputs of a softmax
        layer.**
    -   *Write pseudocode* for a linear layer with bias. (lecture)

-   *Describe* embedding and decoding of embedding

    -   *Describe embedding and decoding of embedding*
        -   ***Describe embedding lookup***
        -   ***Describe how words are selected at output of network***
        -   ***Describe why a linear dimensionality-increasing layer is
            needed at the output***
    -   *Describe* how a Transformer selects the next word of its output
        within the final layer.

-   *Write an executable-clarity definition* (e.g., write pseudocode) of
    the attention operation

    -   *Write an executable-clarity definition (e.g., write pseudocode)
        of the attention operation*
        -   ***Write the equation for the softmax layer?***
        -   ***Write the equation for attention, given softmax? (Q,
            K\^T, V, all together?)   softmax(Q K\^T) V\
            ***
    -   *Describe* how the softmax operation should be normalized, given
        row-ordered or column-ordered data

-   *Describe* the operation of the attention layer.

    -   [*(STRIKE) Describe the operation of the attention
        layer.*]{style="text-decoration: underline;"}
        -   [***Describe key
            matching?***]{style="text-decoration: underline;"}
        -   [***Describe why separate embeddings are needed for each
            input?***]{style="text-decoration: underline;"}
    -   *Describe* how the softmax operation should be normalized, given
        row-ordered or column-ordered data


# Activation Functions etc. -- On Half-Exam 1

-   *Write* pseudocode for the ReLU activation function
-   *Describe* where ReLU is found within the Tranformer \"Attention is
    All You Need\"
-   *Write* pseudocode or *illustrate* the Residual connection
-   *Describe* how residuals improve training of Transformers
-   *Sketch* the feed-forward subnetwork used by Transformer

Attention -- Weeks 1 and 2 -- Including postponed outcomes
----
* *Describe* how one-hot encoding with a fully-connected layer and direct token embeddings are theoretically related. (lecture, 1706.03762)
  * ~~*Describe* how the last linear layer relates to the first embedding layer. (lecture, 1706.03762)~~
* ~~*Explain* how the internal embedding dimension can be varied independent of the vocabulary size. (lecture, 1706.03762)~~

* *Describe* the general structure of a Transformer.
  * *Illustrate* Where embedding, the attention operation, linear layers, and the softmax layer are found within the Transformer. (1706.03762)
  * *Describe* how each operation interacts with those before or after it. (1706.03762)
  * *Describe*, at a very high level, how textual information flows through and is transformed by the Transformer architecture. (This explanation will be able to have more and more detail as you fit in outcomes that come later.) (1706.03762)

* *Write pseudocode* for the input and output layers of a Transformer
  * *Write pseudcode* for the softmax operation. (lecture)
  * *Write pseudocode* for a linear layer with bias. (lecture)
  * ~~*Describe* two different pseudocode interpretations of embedding -- one-hot encoding and direct lookup. (lecture)~~
  * ~~*Write pseudocode* for embedding lookup (lecture)~~

* *Describe* embedding and decoding of embedding
  * ~~*Explain* why one-hot encoded vocabulary words can have significant reduction to their embedded vectors without losing information.~~
  * *Describe* how a Transformer selects the next word of its output within the final layer.

* *Write an executable-clarity definition* (e.g., write pseudocode) of the attention operation 
  * ~~*Re-write* the softmax equation to compute attention on row-ordered data~~
  * *Describe* how the softmax operation should be normalized, given row-ordered or column-ordered data
  * ~~*Re-write* the multihead attention equation for row-ordered data~~

* *Describe* the operation of the attention layer.
   * ~~*Illustrate* a **convex hull** or **simplex** of a set of embedded vectors~~ (lecture,[Youtube Apple Disambiguation](https://www.youtube.com/watch?v=UPtG_38Oq8o))
   * *Define* "A region C as convex if, for all x and y in C, the line segment connecting x and y is included in C." https://en.wikipedia.org/wiki/Convex_set

   * *Define* "A convex combination of the points $x_1, x_2, x_3,$ etc. as a point of the form

   $\alpha_{1}x_{1}+\alpha_{2}x_{2}+ \cdots +\alpha_{n}x_{n}$

   where the real numbers α i \alpha _{i} satisfy \alpha_{i} \geq 0 and \alpha_{1}+\alpha_{2}+\cdots +\alpha_{n}=1."
  * "The Convex Hull is the set of all convex combinations of points in X"
  * ~~*Explain* why the output of a single attention layer must be within the **simplex** of the embeddings of the words entering it.~~ (lecture,[Youtube Apple Disambiguation](https://www.youtube.com/watch?v=UPtG_38Oq8o))
  * ~~*Describe* the Big-O runtime (floating-point multiplications) of a single Attention layer in terms of the dimensionality of the embedding, $d$, and the number of words (tokens) in the input n.~~ (1706.03762)
  * ~~*Define* the dot product geometrically in terms of the lengths of the vectors the and angle between them.~~ (lecture,[Youtube Apple Disambiguation](https://www.youtube.com/watch?v=UPtG_38Oq8o))
  * ~~*Describe* the roles of query and key embeddings in matching distant words within a text.~~  (lecture,[Youtube Apple Disambiguation](https://www.youtube.com/watch?v=UPtG_38Oq8o))


Attention Outcomes we have NOT discussed yet as of Week 2 Friday
----
* *Explain* how a word-piece model overcomes the large-vocabulary challenge.
  * *Describe* how a word-piece model is built from a training set. (1508.07909)
  * *Compare and contrast* words and tokens.

* *Write pseudocode* for a network with skip connections (where a sub-layer's output is "added to the sub-layer input"). These are called residual connections. (1706.03762)

* *Explain the role* of residual connections with a neural network. (1512.03385)

* *Explain* the trade-off between generalization and compute performance seen in mixture-of-experts networks. (Rumor has it GPT-4 is a mixture-of-experts model with 16 experts, with two experts selected, with a total of 1.8 trillion parameters and 120 layers.)

* *Describe* why neural networks are more intuitive, rather than rational, in constructing their outputs (e.g., [Olypiad Geometry], especially blog-post version)

* *Explain* why multi-head attention is conducive to parallel implementsions ([Youtube Summary](https://www.youtube.com/watch?v=TQQlZhbC5ps))


## Attention Outcomes that were NOT within scope for Half Exam 1

-   (NOT ON EXAM) Explain how attention allows for a context to be
    formed around a given token
-   (NOT ON EXAM) Describe the problem with ReLU that Swish addresses
    -- the "dying ReLU problem"
-   (NOT ON EXAM) Describe why nonlinear activation functions are
    needed.
-   (NOT ON EXAM) Describe what is degradation when it applies to deep
    learning and neural networks: Why adding layers HURTS performance.
    "When deeper networks are able to start converging, a degradation
    problem has been exposed: with the network depth increasing,
    accuracy gets saturated (which might be unsurprising) and then
    degrades rapidly. Unexpectedly, such degradation is not caused by
    overfitting, and adding more layers to a suitably deep model leads
    to higher training error, as reported in \[11, 42\] and thoroughly
    verified by our experiments. Fig. 1 shows a typical example."  This
    is what motivated "identity" layers -- "degredation" is a good
    keyword linking to discussion in this paper that several students
    caught and said they could explain that I skipped over until
    randomly seeking outcomes for this exam.

* *Describe* how linear layers learn the structure of the data
* *Describe how* embeddings can have a unique encoding for every word in the vocabulary even though they use a lower-dimensional space
  * Embedding layers learn how to encode the input into a lower dimension space where similar concepts have similar embeddings; this compression helps learning
  * Discuss that one-hot encodings (where there is one dimension for each token in the vocabulary) use only one point along each axis.  There are so many points off of these axes.
  * Discuss why the entire space cannot be used -- points still must be on the "outside" for softmax to decode them later.
  * Run numeric examples


4. Explain the operation of linear and embedding layers.
  * A linear layer transforms the input data using a linear transformation. Values such as weights or biases help to define this process. Embedding layers convert the input and output tokens into vectors. Each token corresponds to a learned vector which the embedding layer then produces. The remaining layers in the transformer can then use the produced vector for each token to perform whatever operations are necessary.

5. Explain why the computation of the network doesn't necessarily grow linearly with the vocabulary at its core.
  * During the embedding process, tokens are mapped to vectors, capturing relationships, and allowing functionally similar words, such as 'dog' and 'canine,' to share the very similar vectors. This combination of parameters effectively reduces the computational growth with vocabulary size. The transformer model learns to treat words with similar meanings as similar vectors, contributing to a more scalable approach.

References
----------
Wherever possible, this document uses references to arxiv papers.  The URL to an arxiv paper can easily be extracted from the number. For example, the URL to the arxiv article arxiv:1508.07909 is https://arxiv.org/pdf/1508.07909.pdf
