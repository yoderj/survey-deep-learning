
# WordPiece

* *Give examples* of problems with a fixed-vocabulary model that a WordPiece model can solve.
* *Describe* the steps of the algorithm for constructing a WordPiece encoder
  * *Describe* the initial vocabulary used by the WordPiece model before tokens start to be merged.
  * *Describe* the criterion used to merge two WordPiece units into one.
* *Give an example* of what the wordpieces might look like for a short input.

# Optional WordPiece Outcomes

* *Give examples* of problems with a fixed-vocabulary model that a WordPiece model can solve.
  * *Describe* what is meant by an out-of-vocabulary word.
  * *Describe* what is meant by a rare word.
* *Give an example* of why a single word-piece embedding may embed multiple languages.

# Outcomes that go beyond the current readings

* *Describe* an algorithm for generating a deterministic segmentation for any possible sequence of characters, given the completed WordPiece tokn dictionary
* *Describe* the BLEU score
* *Describe* what a residual connection is
* *Describe* how model accuracy changes with a changing vocabulary size \[[Pretraining 16 language models on different tokenizers](https://github.com/alasdairforsythe/tokenmonster/blob/main/benchmark/pretrain.md)\]
