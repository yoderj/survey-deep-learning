Outcomes for Quiz 6 (Diffusion):

* Given an image that is total noise, describe the process of producing a denoised image.
* Describe how the simple denoising algorithm works.
  * The simple algorithm: Blended average of denoised and noised image.
* Describe the responsibility of the denoise() function used in the first code sample of this post and illustrated in Fig. 2 of the blogpost [Demystifying Diffusion-Based Models](https://developer.nvidia.com/blog/generative-ai-research-spotlight-demystifying-diffusion-based-models/)
  * What the network learns: To simply recover the image to which a known amount of noise was added
* Describe the relationship between the squiggly lines and the smooth gradients in Figure 4. (an animated figure)
  * Example correct answer: "The lines are the diffusion paths for individual images whereas the smooth gradients are the distribution of diffusion paths"
* Explain the difference between stochastic denoising and deterministic denoising
  * Describe the difference between the denoising algorithm illustrated in Figure 5 and Figure 6.
    * "The function in Figure 5 uses randomness with some extra forces to stay within the distribution, while the function in Figure 6 follows the gradient smoothly to the left without additional noise."  
  * Describe the difference between using a stochastic differential equation and an ordinary differential equation to guide diffusion.
    * The SDE adds noise along with removing noise at each timestep and follows more naturally from the $p()$ and $q()$ equations we considered earlier.
    * The ODE only removes noise, leading to a deterministic algorithm where every noisy image corresponds to only one real image.
  * Describe how the black arrows in, e.g., Figure 10, relate to the denoise() algorithm
