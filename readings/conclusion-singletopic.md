# Outcomes

End your reflection with two lists of outcomes.

**Please format your outcomes in the standard format** used, for example, in our [attention outcomes file](https://gitlab.com/yoderj/survey-deep-learning/-/blob/main/outcomes/attention.md). Each outcome should start with (or quickly get to) a simple verb in the imperative (commanding) format, then follow with a completion of a description of an action you can do (or want to be able to do).  For example, "***Compare*** the advantages of the Adam, Adagrad, and SGD optimizers."

**O1. Write three or more outcomes for things you are comfortable doing after reading this paper.**

**O2. Write one to three outcomes for things you think are important to be able to do after reading this paper, but which you cannot do yet.**

After these outcomes, include a brief statement that you did not use an LLM (like ChatGPT) *or* a brief description of how you used it -- e.g., to revise your own writing, or to prepare your initial draft.  **If you use ChatGPT, share a link to your chat discussions within the reflection.**  My goal is for you to engage with the readings.  Using ChatGPT to generate your entire reading reflection is allowed but **not** encouraged -- and unlikely to work well!

Please feel free to link to other papers or blog articles that you referenced while considering the reading.
