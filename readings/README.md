This folder contains the source text for the readings.

To facilitate consistency in the instructions from reading to reading, standard introductory and concluding texts are used for all readings.

This script `build` puts all the readings into one place.

Run `./build technical/alexnet` from this folder to create the file `technical/alexnet.html`.
Run `./build ethics/ethnicbias` from this folder to create the file `ethics/ethnicbias.html`.

See a more complete plan of readings for the term in `reading_list_24s2.md`
