
Suggestion for future terms: Bring `linear_attention.md` homework earlier int he term.

1. technical/attention.md
2. technical/wordpiece.md
3. technical/residual.md (And feed-forward sub-layers)
4. `homework/linear_attention.md`
5. technical/positional-encoding.md 
6. technical/multihead.md
7. technical/beam-search.md


