Please provide a peer review on last week's reading as early in this week as possible, but at the latest, by the deadline for this reading.  For your peer review, provide a comment that EITHER mentions one thing you appreciate about the other student's reflection OR suggests one thing that could be improved.

While reading, write a brief report including the date, the course number (CS4980Y), and the reflection number and title (e.g., Reflection 1: Attention is All You Need).

Do NOT write your name in the header of the file.  This is to make the peer review double-blind. "The reviewer cannot see the name of the assigned person whose work is to be reviewed, and the student who submitted the assignment cannot view names associated with any comments." ([Canvas instructor docs](https://community.canvaslms.com/t5/Instructor-Guide/How-do-I-use-peer-review-assignments-in-a-course/ta-p/697))

The peer review assignment should show up under modules right under the original assignment with an angled right arrow pointing to the indented peer review assignment.  For other ways to find the peer reviewed assignment, or for instructions on how to submit a peer review, please refer to the [Canvas student docs](https://community.canvaslms.com/t5/Student-Guide/How-do-I-submit-a-peer-review-to-an-assignment/ta-p/293)

Copy the bold questions into your report as mini section-headers and write your answers under each heading in regular font.

Be sure to remember to include your lists of outcomes at the end of your report, clearly distinguished from your answer to the last question -- see more details later in this assignment.

Also remember to include a brief statement about ChatGPT's contributions to your work -- again, see details at the end of the assignment. 

I expect you to go deep, not wide, as you read the papers. I do *not* expect you to read the entire technical articles, just to navigate them and read pertinent paragraphs in search of the answers to the questions or outcomes of interest to you.  But I *do* expect you get practical chunks of knowledge out of the papers, and this will mean reading parts of the papers closely.  You may find that you wish to return to these articles later as other questions come to you throughout your career.

I prefer rough statements in your own words to exact phrases borrowed from the literature.  Try to describe phenomena in a way that goes beyond simply repeating jargon from the literature and instead describes how you see those phenomena.  Limit yourself to at most one or two very brief quotations.  Be sure to mark these with quotation marks if you use them.  I do not mind minor stylistic errors in your prose and prefer these to something polished by ChatGPT. 

You can see the results of the "plagiarism" tests.   Please leave a significant amount of time to completely rewrite any sections identified by the tests.  My goal is that you will be able to write text that does not include "plagiarism" without the need of the tool, but the tool provides important feedback for learning this skill in the beginning.

