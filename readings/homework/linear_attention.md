---
title: "(Mostly) Linear Attention"
---

Attention is a high-dimensional linear operation with only one nonlinearity: Softmax.

In this homework assignment, you will practice the linear algebra needed to do softmax.

Given the significant amount of mathematics, I recommend you work -- and submit -- this assignment on paper.  It's just easier that way.

If you have enginering grid paper, I recommend using it.  You can also use lined or unlined paper.

Write the question numbers below into your assignment.  You do NOT need to repeat the questions.

**1.**

*Compute* the matrix multiplication

$$\left[\begin{array}[cc]
x1&&0\\
0.5&&0.5
\end{array}\right]
\left[\begin{array}[ccc]
x1&&2&&3\\
3&&2&&1
\end{array}\right] $$

**2.**

*Describe* how your answer to **1.** can be interpreted as a convex combination of the rows of the right matrix. That is, describe verbally your intuition of how the rows are combined.  You don't need to repeat any numbers, but may.

 (See Desmos demos on Convex Combinations.)

**3.**

*Compute* the matrix multiplication

$$
\left[\begin{array}[ccc]
x1&&2&&3\\
3&&2&&1
\end{array}\right]
\left[\begin{array}[ccc]
x1&&0&&0.33\\
 0&&1&&0.33\\
 0&&0&&0.33
\end{array}\right] 
$$

**4.**

*Describe* how your answer to **3.** can be interpreted as independent operations on each row of the right matrix.

**5.**

*Compute* the matrix multiplication

$$
\left[\begin{array}[cc]
x1&&1\\
1&&-1\\
-1&&-1\\
\end{array}\right]
\left[\begin{array}[ccc]
x1&&1&&-1\\
1&&-1&&-1
\end{array}\right]
$$

**6.**

*Describe* how your answer to **3.** can be interpreted as dot products of 
the rows of the matrix on the left and columns of the matrix on the right.

Think about the angles between the vectors and recall the definition of the 
dot product that $a \cdot b = ||a|| ||b||\cos \theta$ 
 where $\theta$ is the angle between vectors $a$ and $b$.

**7.**
The softmax operation is $softmax(Q K^T) V$.  Focus on $Q K^T$. 

Select one: This operation is:

1. a convex combination of embeddings
2. operations that change each embedding individually
3. pairwise dot product between all embeddings

**Demonstrate** your answer. Make your answer detailed enough to capture your full thinking.


**8.**

Focus on $A V$, where $A = softmax(Q K^T)$


Select one: This operation is:

1. a convex combination of embeddings
2. operations that change each embedding individually
3. pairwise dot product between all embeddings

**Demonstrate** your answer. Make your answer detailed enough to capture your full thinking.

**9.**

The first linear layer within the feed-forward sublayer computes $E W$

Select one: This operation is:

1. a convex combination of embeddings
2. operations that change each embedding individually
3. pairwise dot product between all embeddings

**Demonstrate** your answer. Make your answer detailed enough to capture your full thinking.


# Outcomes

End your reflection with two lists of outcomes.

**Please format your outcomes in the standard format** used, for example, in our [attention outcomes file](https://gitlab.com/yoderj/survey-deep-learning/-/blob/main/outcomes/attention.md). Each outcome should start with (or quickly get to) a simple verb in the imperative (commanding) format, then follow with a completion of a description of an action you can do (or want to be able to do).  For example, "***Compare*** the advantages of the Adam, Adagrad, and SGD optimizers."

**O1. Write three or more outcomes for things you are comfortable doing after working through this assignment.**

**O2. Write one to three outcomes for things you think are important to be able to do after working through this assignment, but which you cannot do yet.**

After these outcomes, include a brief statement that you did not use an LLM (like ChatGPT) *or* a brief description of how you used it -- e.g., to revise your own writing, or to prepare your initial draft.  **If you use ChatGPT, share a link to your chat discussions within the reflection.**  My goal is for you to engage with the readings.  Using ChatGPT to generate your entire reading reflection is allowed but **not** encouraged -- and unlikely to work well!

For your handwritten submissions, please use a bitly link rather than the full OpenAI URL.

Please feel free to link to other papers or blog articles that you referenced while considering the reading.

