title: "Reading: Latent diffusion"
# Latent Diffusion

Now that the basics of diffusion are starting to feel a bit more comfortable, let's look at a paper that achieved the state of the art in text to image translation in Summer of 2022: The latent diffusion model. (The paper was originall submited as a preprint to Arxiv Dec 2021, and the same architecture used to train a larger model later that year.)

One of the reasons that Latent Diffusion stands out is that it was produced in collaboration with open research groups, trained on open data ([LAION - large-scale AI open network](https://laion.ai/), and released [source code](https://github.com/CompVis/latent-diffusion) with an open (MIT) license.

We will examine the preprint paper itself, [High-Resolution Image Synthesis with Latent Diffusion Models](https://arxiv.org/pdf/2112.10752.pdf), in this week's reading.

Consider the intro to Section 3 on p. 3 and look at Figure 2 on p.2.  Here, DMs are Diffusion Models.

## The latent space

**1. What are the two phases of image compression that motivate latent diffusion instead of image diffusion?**

Now consider Section 3.1.  

**2. Consider an image of size 256x256 and an $f$ of 4.  What spatial resolution will the latent "image" have?**

Your answer to question 2 *should* differ greatly from the wild guesses I shared in class.

Now consider Section 4.1 on p. 5 and Figure 6 on p. 6.  

**3. What values of f lead to lower (better) [FID scores](https://en.wikipedia.org/wiki/Fr%C3%A9chet_inception_distance)?**

Now consider Section E.2.1 on p. 24-25, especially Table 15.

**4. What $f$ and $z$-shape are used for Text-to-Image translation with the LAION dataset?**

For reference, the image shape is likely 256 x 256 x 3, as for LDM-1 in Table 14.

Note that your answer to question 4 should be different than my wild guesses shared in class.

## The diffusion model

Consider the last sentence of section 3.2, "Since the forward process is
fixed, $z_t$ can be efficiently obtained from $\mathcal{E}$ during training,
and samples from $p(z)$ can be decoded to image space with
a single pass through $\mathcal{D}$."

**5. Briefly describe how a sample $z_t$ would be generated, listing the most important two or three steps**

(You answer question 5 based on our previous experience with diffusion models, not from what this paper describes.)

Consider section 3.3. As we discussed in class, there may be a few typos in this section (in particular, the W's likely should be multiplied on the right, not on the left.)

**6. What is the dimension of the attention matrix ($\mathrm{softmax}\left(\frac{QK^T}{\sqrt{d}}\right)$)**

This matrix is NOT square.

**7. What do the rows and columns of this matrix correspond to?**

