title: "Reading: Multi-head attention"

# Multi-head attention

Review Figure 2 and section 3.2.2 of [the "Attention is all you need" paper](https://arxiv.org/pdf/1706.03762.pdf).

Skim through [the "multihead attention" chapter of the Dive into Deep Learning online textbook](https://d2l.ai/chapter_attention-mechanisms-and-transformers/multihead-attention.html)

**1. Each attention head has three fully-connected layers entering it.  What are the INPUT dimensions of these INPUT layers for the base configuration of the model?**

**2. What are the OUTPUT dimensions of these INPUT layers?**

**3. Why does the value of one head's output differ from another's?**

**4. How are the outputs from all the heads combined?**

Now consider the linear transformation which is Equation 11.5.2 in [the d2l.ai text](https://d2l.ai/chapter_attention-mechanisms-and-transformers/multihead-attention.html#model) or, if you prefer, the $W^O$ variable in the first equation at the top of p. 5 in the [the "Attention is all you need" paper](https://arxiv.org/pdf/1706.03762.pdf).

**5. What are the input and output dimensions of this linear layer?**

**6. What do you think the purpose of this layer is?**

