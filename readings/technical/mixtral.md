title: "Reading: Mixtral 7B"
# The mixtral model and its predecessors

The line of transformers from the original transformer \[[1706.03762](https://arxiv.org/pdf/1706.03762.pdf)\] to Mixtral is refreshingly short.

* *LLaMA: Open and Efficient Foundation Language Models* \[[2302.13971](http://export.arxiv.org/pdf/2302.13971)\]: The Llama model is Facebook's first public model.  It lists three changes from the original transformer architecture (Section 2.2):
  * Pre-normalization. Normalization before instead of after each layer
  * SwiGLU activiation (instead of ReLU activiation) (and a 2/3 smaller hidden layer in the feed-forward block)
  * Rotary Embeddings. Instead of adding positional encodings once at the input, positional encodings are added at each layer of the network.  (RoPE, \[[2104.09864.pdf](https://arxiv.org/pdf/2104.09864.pdf)\])
* *Llama 2: Open Foundation and Fine-Tuned Chat Models* \[[2307.09288](https://arxiv.org/pdf/2307.09288.pdf)\]: The Llama 2 model adds one notable architectural change:
  * Section 2.2, Appendix A.2.1 (p. 47): Grouped-Query Attention: Keys and Values for a token can be cached and reused when that same token appears in later processing. However, this starts to require significant memory. Thus, it is common to reduce the number of keys and values used, reusing the same key-value pairs for several heads.  Llama settles on a variant of key-value sharing known as GQA (Grouped-query attention, \[[2305.13245](https://arxiv.org/pdf/2305.13245.pdf)\]
* *Mistral 7B* \[[2310.06825](https://arxiv.org/pdf/2310.06825.pdf)\]: 
  * Mistral uses a clever rotating context window that uses the space above the diagonal to hold more context for the earlier tokens in a sequence.  Because each layer builds on the tokens before it, this allows each layer to reach back one more context length into the past, allowing for $k \times W$ tokens to be considered at the output for a network with a context length of $k$ and a context window in each layer of $W$ tokens.  (Context lengths are limited in part because of using RoPE for the emedding intead of absolute positional embeddings)
* *Mixtral of Experts*: \[[2401.04088](https://arxiv.org/pdf/2401.04088.pdf)\] 
  * Mixtral uses nearly an identical architecture to Mistral, except that the feed-forward layers use a mixture of experts. Thus, only a couple of a pool of feedforward networks are selected for each forward pass instead of just using one feed-forward network.
  * It is interesting that these experts are used for the feed-forward layer, not for the attention layer.  This suggests that the feed-forward layer has an important role in memorizing large corpora of text.


**1. *Explain* why there can be more Q than K values when using GQA**

**2. *Describe* which part of the Mixtral network uses mixture-of-experts**

**3. *Enumerate* how many experts are used within a layer for each token, and how many experts total are available within Mixtral**

You can play with the Mixtral model for free (currently) here: [Groq playground](https://console.groq.com/playground)
