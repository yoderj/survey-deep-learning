title: "Reading: Beam Search"

Read ([d2l.ai: Section 10.8: Beam Search](https://d2l.ai/chapter_recurrent-modern/beam-search.html#id1)) and all of its subsections.  You will see that Subsections 10.8.1 and 10.8.2 review what we discussed about generating likely sequences instead of likely tokens, and 10.8.3 describes the beam search algorithm.

While reading, ignore the bold condition variable $\mathbf{c}$. This is a latent variable that would be brought over from an encoder to the decoder. But a decoder-only transformer such as we have been using this term does not need this term -- the rest of the discussion applies directly to transformers.

In Section 10.8.1, the expression for the likelihood of sequences can be mapped using the definition of conditional probability $P(A|B)P(B) = P(A,B)$ to show that $\prod_{t'=l}^{T'} P(y_{t'} \mid y_{1}, \ldots, y_{t'-1}, \mathbf{c})$ = $\prod_{t'=1}^{T'}P(y_{l},y_{l+1},y_{l+2},y_{l+3},...,y_{T'} \mid y_{1}, y{2}, ... y{l - 1})$. In other words, the likelihood of a sequence is simply the probability of selecting ALL the specific worsk from $y_{l}$ through $y_{l+T'}$. (I modify the equation from the text to start at' at l instead of 1, since presumably there are many words from locations 1 through l-1 before we start using the t index.)

**1. Describe what beam size $k$ indicates in the beam search algorithm.**

**2. Describe how the probability P(C,E,D) is computed in the example.**

**3. Do you think it is possible for the beam search algorithm to have two final output candidates that start with the same beam-selected words? Why or why not?**

