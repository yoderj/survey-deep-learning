title: "Reading: Demystifying Diffusion-Based Models"
# Demystifying Diffusion-Based Models

Our reading this week is the NVIDIA blog-post [Demystifying Diffusion-Based Models](https://developer.nvidia.com/blog/generative-ai-research-spotlight-demystifying-diffusion-based-models/)

**1. Describe the responsibility of the `denoise()` function used in the first code sample of this post and illustrated in Fig. 2**


**2. Describe the relationship between the squiggly lines and the smooth gradients in Figure 4. (an animated figure)**

**3. Describe the difference between the denoising algorithm illustrated in Figure 5 and Figure 6.**

**4. Describe how the black arrows in, e.g., Figure 10,relate to the `denoise()` algorithm**

