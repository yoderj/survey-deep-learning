title: "Reading 1: Attention is all you need"
## Attention Is All You Need

The paper for our first reading is: [Attention is all you need](https://arxiv.org/pdf/1706.03762.pdf)

This paper does not define the concept of attention, which was already common in the literature before this point.  But it does define "scaled dot-product attention" which is the form of attention central to all Transformers, including ChatGPT, to this day.

1. **Describe a goal of the softmax layer.**

2. **Explain the operation of linear and embedding layers.**

3. **Explain why the computation of the network doesn't necessarily grow linearly with the vocabulary at its core?**

The following questions are just for fun -- not for credit:

4. **Does the paper use a row-ordered or column-ordered representation for data?**

(That is, is a single word's input embedding represented as a row or as a column?)

5. **Does the softmax normalize across the rows, or across the columns?**

(That is, does attention sum to one along the rows, or along the columns?)

6. **What is the V matrix for?**
