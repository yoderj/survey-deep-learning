title: "Reading: Overview of Diffusion Papers" 
# Overview of this reading

This reading will be similar in some ways to our first reading of the attention is all you need paper.

I do *not* expect you to understand the vast majority of what you read in the papers by the time you submit this reading.

And there are NO guided questions on the readings. You will only submit outcomes this week.

But unlike the previous reading, we have MANY papers associated with this reading.  **You only need to pick one or two of them to skim through this week.**

I don't expect you to feel like you understand anything solidly at the end of the readings, but I'm hoping you can scrape out one broad outcome that summarizes something you picked up from the reading.  This is *not* likely to make its way into half exams or quizzes.

# The core papers

During the year 2023, two image generation models became very popular. These were DALL-E 2 and Stable Diffusion.  Open AI released access to the DALL-E 2 model, at first for free, and then as a paid subscription. Stable Diffusion was released by Stability AI as an open-source model.  Here are the papers:

* DALL-E 2: [Hierachical Text-Conditioned Image Generation with CLIP Latents](https://arxiv.org/pdf/2204.06125.pdf) -- Technically, the architecture described in this paper is called "unCLIP" by OpenAI, but it is widely believed to be the architecture of DALL-E 2 as well.
* Stable Diffusion: [High-Resolution Image Synthesis with Latent Diffusion Models](https://arxiv.org/pdf/2112.10752.pdf) -- The latent diffusion architecture defined in this paper is exacty the architecture used for Stability AI's [stable diffusion](https://github.com/Stability-AI/StableDiffusion) as well. 

Most of us hadn't even heard of diffusion before these models entered the scene, but research had been going on in the field for some time.  The next section shares ALL the papers I'm aware of in this area.

# Highlighted stable diffusion papers

As I mentioned in class, I will present diffusion from the perspective of "denoising" rather than a "score-based" approach.  I select the papers below that appear most central to that narrative. Here, the order isn't chronological, but rather in the order that one idea appears to build upon another.

* [June 2020 - Denoising Diffusion Probabilistic Models](https://arxiv.org/pdf/2006.11239.pdf)
  * [PyTorch tutorial implementing the Denoising Diffuusion paper](https://nn.labml.ai/diffusion/ddpm/index.html)
* [Mar 2021 - Learning Transferable Visual Models From Natural Language Supervision](https://arxiv.org/pdf/2103.00020.pdf) -- the CLIP paper.  The CLIP architecture maps both images and text into the same latent space, and is essential for guiding diffusion models with text.
* [Dec 2021 - Classifier-Free Diffusion Guidance](https://openreview.net/pdf?id=qw8AKxfYbI) 
  * [July 2022 - Classifier-Free Diffusion Guidance](https://arxiv.org/pdf/2207.12598.pdf) -- The arxiv version
* [Dec 2021 - High-Resolution Image Synthesis with Latent Diffusion Models](https://arxiv.org/pdf/2112.10752.pdf) - *the* latent diffusion paper from Stability AI. Mentioned above.
  * [Repo: Stable diffusion](https://github.com/CompVis/stable-diffusion?tab=readme-ov-file)
  * [blog on "Latent Diffusion Models" paper](https://www.casualganpapers.com/high-res-faster-diffusion-democratizing-diffusion/Latent-Disffusion-Models-explained.html)
  * [Some latent diffusion blog?](https://ommer-lab.com/research/latent-diffusion-models/)
  * [blog post providing annotations to the core figure of this paper](https://towardsdatascience.com/what-are-stable-diffusion-models-and-why-are-they-a-step-forward-for-image-generation-aa1182801d46)
  * [Wiki: Stable Diffusion](https://en.wikipedia.org/wiki/Stable_Diffusion) - Explains that Latent Diffusion *is* stable diffusion
* [Apr 2022 - Hierarchical Text-Conditional Image Generation with CLIP Latents](https://arxiv.org/pdf/2204.06125.pdf) - The DALL-E 2 paper? Mentioned above.

# All my stable diffusion papers

This is a brief review of ALL my stable diffusion papers.  We won't be reading all of these together this term, but I want you to have access to the same papers I do at this point.

Here is my informal literature review on diffusion:  Dates are the dates the preprints were posted to arxiv, which is often six months before the more official peer-reviewed conference.

It is interesting to see how many papers contribute significantly to this field over the course of just a couple years.

* [2009 - Learning Multiple Layers of Features from Tiny Images](https://www.cs.toronto.edu/~kriz/learning-features-2009-TR.pdf)
* [2011 - A Connection Between Score Matching and Denoising Autoencoders](https://www.iro.umontreal.ca/~vincentp/Publications/DenoisingScoreMatching_NeuralComp2011.pdf)
* [May 2019 - Sliced Score Matching: A Scalable Approach to Density and Score Estimation](https://arxiv.org/pdf/1905.07088.pdf)
* [July 2019 - Generative Modeling by Estimating Gradients of the Data Distribution - mentions Langevin dynamics](https://arxiv.org/pdf/1907.05600.pdf)
* [June 2020 - Improved Techniques for Training Score-Based Generative Models](https://arxiv.org/pdf/2006.09011.pdf)
* [June 2020 - Denoising Diffusion Probabilistic Models](https://arxiv.org/pdf/2006.11239.pdf)
  * [PyTorch tutorial implementing the Denoising Diffuusion paper](https://nn.labml.ai/diffusion/ddpm/index.html)
* [Feb 2021 - Zero-Shot Text-to-Image Generation - uses a VAE and a transformer](https://arxiv.org/pdf/2102.12092.pdf) -- DALL-E 1 -- Open AI, a GAN, not a diffusion model.
* [Mar 2021 - Learning Transferable Visual Models From Natural Language Supervision](https://arxiv.org/pdf/2103.00020.pdf) -- the CLIP paper.  The CLIP architecture maps both images and text into the same latent space, and is essential for guiding diffusion models with text.
* [May 2021 - Diffusion Models Beat GANs on Image Synthesis](https://arxiv.org/pdf/2105.05233.pdf) -- OpenAI. The title says it all.
  * [Youtube video about Diffusion Models Beats GANs paper](https://www.youtube.com/watch?v=W-O7AZNzbzQ)
* [June 2021 - Score-based Generative Modeling in Latent Space](https://arxiv.org/pdf/2106.05931.pdf) - Precursor to latent diffusion?
* [Dec 2021 - Towards Photorealistic Image Generation and Editing with Text-Guided Diffusion Models](https://arxiv.org/pdf/2112.10741.pdf) - GLIDE -- an OpenAI DALL-E 2 precursor.
* [Dec 2021 - High-Resolution Image Synthesis with Latent Diffusion Models](https://arxiv.org/pdf/2112.10752.pdf) - *the* latent diffusion paper from Stability AI. Mentioned above.
  * [blog on "Latent Diffusion Models" paper](https://www.casualganpapers.com/high-res-faster-diffusion-democratizing-diffusion/Latent-Disffusion-Models-explained.html)
  * [blog post providing annotations to the core figure of this paper](https://towardsdatascience.com/what-are-stable-diffusion-models-and-why-are-they-a-step-forward-for-image-generation-aa1182801d46)
  * [Wiki: Stable Diffusion](https://en.wikipedia.org/wiki/Stable_Diffusion) - Explains that Latent Diffusion *is* stable diffusion
  * [The Stable Diffusion repo](https://github.com/CompVis/stable-diffusion)
  * [Some latent diffusion blog?](https://ommer-lab.com/research/latent-diffusion-models/)
* [Dec 2021 - Classifier-Free Diffusion Guidance](https://openreview.net/pdf?id=qw8AKxfYbI) - * [](https://github.com/CompVis/stable-diffusion?tab=readme-ov-file)
  * [July 2022 - Classifier-Free Diffusion Guidance](https://arxiv.org/pdf/2207.12598.pdf) -- Arxiv version
* [Apr 2022 - Hierarchical Text-Conditional Image Generation with CLIP Latents](https://arxiv.org/pdf/2204.06125.pdf) - The DALL-E 2 paper? Mentioned above.

A review of the score-based literature (which I plan to NOT teach you this term):
* [Dr. Bouman's "Generative Models" slides on Langevin dynamics](https://engineering.purdue.edu/DeepLearn/pdf-bouman/DL-week-15.pdf)

A video tutorial recommended by a student:
* [How Stable Diffusion Works](https://www.youtube.com/watch?v=sFztPP9qPRc)

