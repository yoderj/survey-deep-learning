This week's reading will be to complete [this Python notebook](positional-encoding.ipynb).

Please access the notebook through Rosie.  Look for an email from me giving you instructions on how to log into Rosie for the first time.  That email will link back to this lab.

Once you are able to log into Rosie, navigate in your browser to the [My Interactive Sessions page](https://dh-ood.hpc.msoe.edu/pun/sys/dashboard/batch_connect/sessions).  On that page, in the pane on the left, under "Interactive Apps / Project Jupyter Servers section," select **Jupyter Lab - Rosie**. It is important that you use Jupyter Lab for the best experience using this notebook.

You can use 0 GPUs for this lab. Select some number of **hours greater than 1**. (You will suddenly lose some unsaved work once this timer times out -- pick something longer than you need.).  Click **Launch**. Refresh the page after a couple of seconds. Click on the **Connect to Jupyter** button.

Click on the upload button (an arrow pointing up) to upload the provided notebook to Rosie. You can also create subfolders through the Jupyter Lab interface if that seems useful to you.

Click on the notebook to load your notebook.

Run the first cell (`!python3 -m pip install jupyterlab-hide-cells`). If you are ssh'd into Rosie still, you can also run this command from the shell prompt.

Now go back to the  [My Interactive Sessions page](https://dh-ood.hpc.msoe.edu/pun/sys/dashboard/batch_connect/sessions) and launch another **Jupyter Labs - Rosie** session, just as you did before.  **This is necessary for the best reflection/homework experience**, as there is one challenge in the lab which has a hidden solution which you might see if you don't do this first.  Once it works, right-clicking on a cell should show a "Hide this cell" feature -- a feature you won't use directly, but which tells you it is configured correctly.

At this point, you can start reading through and executing the cells of the assignment.  The assignment has instructions within it on how to fill it out.

When you are satisified with your assignment, **click the save button** (this won't happen automatically!) and then right-click on your file and select Download (there is an arrow pointing down as the icon for this one).  Upload the ipynb file to Rosie.

You will note the standard outcomes and LLM-use questions are embedded into the notebook. Be sure to fill these out.
