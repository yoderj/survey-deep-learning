title: "Reading: WordPiece"
## First Paper: [Google’s Neural Machine Translation System: Bridging the Gap between Human and Machine Translation](https://arxiv.org/pdf/1609.08144.pdf)

INSTRUCTOR TODO:  Link the BPE (byte pair encoding) mention in the Attention paper to this reading.  Point out that BPE and WordPiece are the same algorithm, with only minor variations which must be specified by authors (or by the library being used...)

Look at the introduction to Section 4 and Section 4.1.

1. **What problem does the Wordpiece model solve?**

2. **Give an example where a word may require more than one wordpiece.**

3. **How does the use of special symbol differ from reference [35]**?

## Second paper: [Neural Machine Translation of Rare Words with Subword Units](https://arxiv.org/pdf/1508.07909.pdf)

Look at Section 3.2, Byte Pair Encoding (BPE). The algorithm described as an adaption of Byte Pair Encoding is what is now known as WordPiece.

3. **What is in the initial vocabulary before iteration?**

4. **When merging symbol pairs, what is the criterion for selecting the next pair to merge?**

5. **How is the efficiency improved beyond the simple version of the algorithm shown in Figure 1/Table 1?**

## Optional Papers

* [BERT: Pre-training of Deep Bidirectional Transformers for Language Understanding](https://arxiv.org/pdf/1810.04805.pdf)

BERT is one of the first widely-used transformers to use WordPiece.  You can see their discussion of WordPiece on p. 4 (Sec. 3, subsection "Input/Output Representations").

