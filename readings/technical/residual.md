title: "Reading: Residual Connections and the Feed Forward Sublayer"
# Reading: [AlexNet: ImageNet Classification with Deep Convolutional Neural Networks](https://papers.nips.cc/paper/2012/file/c399862d3b9d6b76c8436e924a68c45b-Paper.pdf)

Focus on Section 3.1

1. ***Define* the ReLU operation. **

Sketch a graph of y=ReLU(x) vs x.  You may optionally include this sketch in your report.

# Reading: [Deep Residual Learning for Image Recognition](https://arxiv.org/pdf/1512.03385.pdf)

Focus on Section 1, especially the first column in p.2 and Figure 2.

2. ***Repeat* the definition of residual layer as given in this paper** 

3. ***Explain* how residual connections make the learning task of a layer easier.**

# Return to [Attention Is All You Need](https://arxiv.org/pdf/1706.03762.pdf)

Focus on Figure 1 and Section 3.1.

4. ***Describe* the two sublayers that replace the convolutions and ReLU in the Attention Is All You Need Paper.**

(You could also sketch a figure here to support your text.)

Now focus on Figure 1 and Section 3.3.  This section describes one of the sub-layers.

5. ***Enumerate* how many fully-connected layers are within the "Feed Forward" sub-layer**

6. ***Name* the activation function used between the two fully-connnected layers in the "Feed Forward" sub-layer.**

7. ***Enumerate* the neurons entering and exiting the "feed forward" layer.**

8. ***Enumerate* the neurons within the inner layer of the feed forward layer.**

(You may wish to optionally sketch a figure here illustrating the dimensions of this layer.)

