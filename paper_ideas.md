This public repository for this course (at https://gitlab.com/yoderj/survey-deep-learning/-/tree/main/) holds many possible papers, but I have other potential papers for interested students available. If you are looking for interesting reading beyond what is required for this course, wanting to see some possible readings that COULD be used in this course, or interested in proposing your own, here are some ideas that give you a sense of the papers that are available.

# Sketches on LLMs from Summer 2024

Beam Search
 * (This week's reading)
   - Wikipedia does NOT describe in the context of an LLM (https://en.wikipedia.org/wiki/Beam_search) - saw a good tutorial going over variants earlier in summer in a library's documentation. (Paper TBD)


Guiding LLMs
 * General concept: Code around the LLM
 * guidance (microsoft library)
     https://github.com/guidance-ai/guidance
 *json former


System 1 vs System 2 Reasoning (see Karpathy 1hr LLM video, 3/4 way through)

CoT (Chain of Thought) Reasoning
 * Basic idea: Guide model to think through chain-of-thought topics
 * 2308.09687v2.pdf (arxiv.org) -- Graph of Thoughts -- compare with Algorithm of Thoughts
 * Variations:
   - ART https://arxiv.org/abs/2303.09014 
   - https://arxiv.org/abs/2210.03493
 * CoT and rationalization of biases: https://arxiv.org/abs/2212.08061
 * Auto-COT https://arxiv.org/abs/2210.03493
 * ART https://arxiv.org/abs/2303.09014

Embedding biases
* https://medium.com/advanced-deep-learning/word-embeddings-and-their-bias-3f30cd22760f
 - TODO: Include more references

Stable Diffusion
 - https://fastsdxl.ai/


Groq - LPU
 - https://groq.com/
 - Mixtral-8x7b
   - Mixture of expert models

Grokking
 * Original paper (TODO: link)
 * Follow-up blogpost/paper from Google: https://pair.withgoogle.com/explorables/grokking/ 


OpenAI video stuff
 - multiple frames at a time

Google's latest network
 - Gemini 1.5 with 1mil token context window
 
 - Stable diffusion
   - https://openai.com/research/video-generation-models-as-world-simulators
   - Sora (also uses a Transformer architecture)
   - DALL-E


Calibration
 * GPT-4 well calibrated before fine-tuning on a specific task:https://arxiv.org/pdf/2303.08774.pdf


A/B testing Ethics:
 * ChatGPT trained with Kenyan workers: https://time.com/6247678/openai-chatgpt-kenya-workers/


https://www.youtube.com/@AndrejKarpathy
* Why we should get rid of WordPiece by AndrejKarpathy https://www.youtube.com/watch?v=zduSFxRajkE



List of lists:
 * Top A.I. Newsletters of 2023 Reloaded (beehiiv.com)
   https://read-last-futurist-newsletter.beehiiv.com/p/top-ai-newsletters-2023-reloaded

Building ML apps:
* https://www.gradio.app/Framework for building interactive ML apps.

AI News lists
* https://thezvi.substack.com/ - Sarcastic reviews of things happening in AI.

Backing up your ChatGPT Chat: https://github.com/abacaj/chatgpt-backup/blob/main/backup.js

For first-time programmers:
https://www.manning.com/books/learn-ai-assisted-python-programming

Free Summer 2024 (for a limited time) short courses: 
https://www.deeplearning.ai/short-courses/



# Required readings
The readings are found in the reading folder. I’m creating the readings each week based on the progress of the class.

I have a script that appends and postpends standard reading requirements around each reading.  If you clone the repo and run 

./build technical/attention

it will build the first reading page.  But you can see the links directly from the markdown files.

In order so far, the readings are:

* https://gitlab.com/yoderj/survey-deep-learning/-/blob/main/readings/technical/attention.md -- Attention is All you Need (defining a transformer’s architecture)
* https://gitlab.com/yoderj/survey-deep-learning/-/blob/main/readings/technical/wordpiece.md -- WordPiece text encoding
* https://gitlab.com/yoderj/survey-deep-learning/-/blob/main/readings/technical/residual.md -- Residual connections in transformers
* https://gitlab.com/yoderj/survey-deep-learning/-/blob/main/readings/homework/linear_attention.md -- A homework assignment exploring how one operation (matrix multiplication) transforms embeddings in three different ways in different parts of a transforer.

# Outcomes

Not yet formed into readings are some potential topics in the outcomes files.  In the top-level folder is a "week-level outcomes" file 

* https://gitlab.com/yoderj/survey-deep-learning/-/blob/main/week-level-outcomes.md 

This file does NOT contain a week-by-week plan. Rather, the left-most aligned bullets are intended to be high level outcomes that might take a week to explore.  This is especially true for the CNN and following sections. I will NOT cover all outcomes in this file during the term.

But, perhaps interesting to you, there are more links to potentially interesting arxiv papers linked from this file on a wide variety of topics.

The .md files in the https://gitlab.com/yoderj/survey-deep-learning/-/tree/main/outcomes folder also contain some more potential papers.

# Readings for MSOE's CSC-4611 Deep Learning

Finally, if you have NOT yet taken Deep Learning, there are likely papers from that course that could be of interest to you:

This is NOT a public repository, but you can request a copy of the readings if you would like:
https://gitlab.com/msoe.edu/private/csc4611/csc4611/-/tree/main/readings/technical

I’m particularly fond of some of the readings in https://gitlab.com/msoe.edu/private/csc4611/csc4611/-/blob/main/readings/technical/advanced.md, such as the Lottery Ticket Hypothesis paper and of https://gitlab.com/msoe.edu/private/csc4611/csc4611/-/blob/main/readings/technical/interpolation.md which gets thinking about how "overfitting" might be the wrong way to think about deep learning.

In addition, the ethics readings are also in the same private repo:
https://gitlab.com/msoe.edu/private/csc4611/csc4611/-/blob/main/readings/ethics/README.md
this README summarizes updates I made to the ethics readings as part of a CREATE grant.

# Or just ask! 

Finally, I maintain a large repository of bookmarks to arxiv papers and other resources. If there is a particular topic that you are looking for technical papers on, I could probably dig something up.  The main topics I've tracked are CNNs that have high performance on AlexNet (like EfficientNet V2 https://arxiv.org/pdf/2104.00298.pdf) and now LLMs (like Mixtral, which is linked in the "week-level" link above, and like https://qwenlm.github.io/blog/qwen1.5/, which also claims better multi-lingual performance than GPT3.5, but doesn’t seem to have a paper.)
