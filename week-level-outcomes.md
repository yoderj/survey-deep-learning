Top-down goals for the course.

Potential week-level outcomes for Survey of Deep Learning

This course takes a top-down perspective on understanding the operation of deep networks.  As we read about neural networks from current research articles, we will not understand all of the details in those articles.  We may not even understand all of the details necessary to support the high-level claims we make about neural networks.  My hope is that the class will have a desire to dig in and further understand the details of some of the more important claims we examine this term.  At the same time, I am hoping that you will embrace the discomfort of partial, high-level knowledge and build your skills in accurately discussing what makes these networks tick.

LLMs
----

* *Describe* how one-hot encoding with a fully-connected layer and direct token embeddings are theoretically related. (lecture, )
  * *Describe* how the last linear layer relates to the first embedding layer.
* *Explain* how the internal embedding dimension can be varied independent of the vocabulary size. 

* *Describe* why neural networks are more intuitive, rather than rational, in constructing their outputs (e.g., [Olypiad Geometry], especially blog-post version)

* *Explain* the trade-off between generalization and compute performance seen in mixture-of-experts networks. (Rumor has it GPT-4 is a mixture-of-experts model with 16 experts, with two experts selected, with a total of 1.8 trillion parameters and 120 layers.)

* *Write pseudcode* for the softmax operation.

* *Write pseudcode* for a linear layer with bias.

* *Describe* two different pseudocode interpretations of embedding -- one-hot encoding and direct lookup.

* *Write an executable-clarity definition* (e.g., write pseudocode) of the attention operation 
  * *Re-write* the softmax equation to compute attention on row-ordered data
  * *Describe* how the softmax operation should be normalized, given row-ordered or column-ordered data
  * *Re-write* the multihead attention equation for row-ordered data

* *Illustrate* a **convex hull** or **simplex** of a set of embedded vectors
* *Explain* why the output of a single attention layer must be within the **simplex** of the embeddings of the words entering it.

* *Describe* the Big-O runtime of a single Attention layer in terms of the dimensionality of the embedding, $d$, and the number of words (tokens) in the input n.


* *Explain* why one-hot encoded vocabulary words can have significant reduction to their embedded vectors without losing information.
  * *Compare and contrast* words and tokens.

* *Explain* how a word-piece model overcomes the large-vocabulary challenge.
  * *Describe* how a word-piece model is built from a training set -- arxiv:1508.07909


CNNs
-----
* *Describe* how CNNs (including GANs) model the space of natural images. [Feature Visualization, Activation Atlas]
* *Describe* how diffusion models model the space of natural images.
* *Describe* the differences between loss functions and performance metrics, and give examples of how each is essential to deep learning.
  * *Explain* the role of a discriminator as a high-level loss function.
  * *Explain why* a CNN classifier makes a good loss function for a generative CNN, but a poor loss function for a visualization of network activations.
* *Describe*, informally, how the information contained within an image changes as it flows through the layers
  * *Describe* the hierachical structure of a CNN's representation of images.
  * *Explain the signifance* of multiple channels within a CNN's representation.
  * *Describe* how the number of channels changes with the number of layers
  * *Compare and contrast* convolution and transpose convolutional layers and networks.
    * *Perform* convolution and transpose convolution for small images.
  * *Give examples* of uses for convoluational and transpose convolutional networks:
    * Classifiers
	* Discriminators
	* Generators
* *Define* style as the distribution of low- and high-level features within an image, ignoring spatial layout.
  * *Describe* how two images with the same style would look similar and different from one another.
  * *Give examples* of real images that would look the same despite having very different L2 norms.
  * *Explain* how the mean and variance of feature channels in a generator influences the image generated.

  * Papers shared on Day 1
    * https://papers.nips.cc/paper/2012/file/c399862d3b9d6b76c8436e924a68c45b-Paper.pdf - AlexNet (CNNs)
	* https://arxiv.org/pdf/1505.04597.pdf - U-Net
    * https://arxiv.org/pdf/1611.07004.pdf - pix2pix
    * https://arxiv.org/pdf/1508.06576.pdf - Style Transfer: [1508.06576] A Neural Algorithm of Artistic Style
    * https://arxiv.org/pdf/1812.04948.pdf - StyleGAN1
    * https://arxiv.org/pdf/1912.04958.pdf - StyleGAN2	
    * https://arxiv.org/pdf/1706.03762.pdf - Transformer -- This became the primary paper for the term
	* https://arxiv.org/pdf/2401.04088.pdf - Mixture of Sparse Expert Models
    * https://arxiv.org/pdf/2209.01667.pdf - Review of Mixture-of-Experts models
	* https://arxiv.org/pdf/2112.10752.pdf - Stable Diffusion
	* https://github.com/CompVis/stable-diffusion - Stable Diffusion


Beam Search
------------

* *Compute* the total probability of a sentence using an LLM
  * *Define* conditional probability as $P(A \mid B) = \frac{P(A, B)}{P(B)}i$
  * *Illustrate* conditional probability in a small, discrete case.
  * *Derive* the formula for total probability given conditional probability: $P(A, B) = P(A | B) P(B)$ 
  * *Interpret* the output of a Transformer as the conditional probability of the next word, given that a word already exists.
  * *Compute* the total probability of a sentence by iteratively applying the total probability formula.
* *Describe* the beam-search algorithm ([d2l.ai: Recurrent](https://d2l.ai/chapter_recurrent-modern/beam-search.html#id1))
  * *Compare* beam-search with greedy search and exhaustive search


Advanced Potential outcomes
---------------------------

* Describe how and why the elements of a vector (or the channels of a feature-map) may interact
  * *Give an example* of why it is too simplistic to think of a single channel representing a single concept in a neural network. *Describe* how each channel can be considered a basis vector with concepts represented by non-basis vectors within this space. [Feature Visualization]


References
----------
Wherever possible, this document uses references to arxiv papers.  The URL to an arxiv paper can easily be extracted from the number. For example, the URL to the arxive article arxiv:1508.07909 is https://arxiv.org/pdf/1508.07909.pdf


[Activation Atlas]:
 * https://distill.pub/2019/activation-atlas/
 
[Olypiad Geometry]: 
Solving olympiad geometry without human demonstrations
 * https://www.nature.com/articles/s41586-023-06747-5
 * Popular blog version: https://deepmind.google/discover/blog/alphageometry-an-olympiad-level-ai-system-for-geometry/
 
[Feature Visualization]:
 * https://distill.pub/2017/feature-visualization/
 * Channel interaction: https://distill.pub/2017/feature-visualization/#diversity-with-optimization

