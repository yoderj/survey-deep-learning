---
title: Potential Mechanisms for Hallucination Detection
---

LLM hallucination is a significant problem within LLMs.  Indeed, some have gone so far as to suggest that LLMs cannot be trusted at all because of their inherent tendancy to hallucinate [[Hallucination is Inevitable:
An Innate Limitation of Large Language Models](https://arxiv.org/pdf/2401.11817)]. As this article suggests, there are times where any answer except "I don't know" must be wrong.  However, it may be possible for LLMs to learn to simply say "I don't know".  One hint that this may be possible comes from the fact that certain concepts learned by an LLM are associated with just one (or a few) nodes in a network, and by editing the weights associated with these nodes, one can correct errors made by the network (e.g., in ["Mass Editing Memory in a Transformer"](https://memit.baulab.info/)).  This suggests that false "knowledge" learned by the network is localized to these nodes, and if the confidence of the "fact" stored by a node could be judged, that could lead to better estimates of the quality of the output when that node is turned on.

There are a few of mechanisms through which this may be achieved.  The first two are related to a node, and the last to information injected into a prompt, either directly or through a RAG system.

## Potential mechanisms for tracking the reliability of a fact stored in a node
The first mechanism is a training mechanism which counts how many training samples contribute to updates to a node's weights.  It is well-known that individual neurons (or sets of neurons) within a network encode various concepts, and that adjusting JUST these neurons can change the "facts" that a network has memorized.

It would require just an additional int per neuron to count how many samples from the training set have updated each node (this count could also be per weight rather than neuron in simplest implementation that increments the counter whenever the gradient is non-zero to a node).

Nodes that are based on too small a sample from the training set could be not trusted -- they haven't received enough data to have confidence that they have properly encoded the knowledge from their training samples.

Furthermore, it may also be possible to efficiently memorize the few samples that ARE used to train small nodes.  These samples could be triggered to be shared through some sort of RAG-like mechanism whenever these nodes contribute in a meaningful way to the output of a network.

Of course, for well-known "facts" that are repeated many times in the training set, it would be impossible to store all of these references and it is hard to imagine that finding and linking the best referenes to these nodes would be practical using simple variations on the algorithm described above.  But these are the "facts" that are more likely to be correct and less likely to need a source for verification.

A second technique that could be used is to count, in addition to how often the node has been updated, how often that node has contributed to correct vs. incorrect answers.  If a node is activated to learn that Arneb is in the constellation Lepus [[source](https://memit.baulab.info/)], but the output of the network is still saying it is in Aquilla, this node may be on the topic of the constellation of Arneb, but most observations will likely still say this node is incorrect in its output, and thus this node should be marked with a low accuracy (when comparing the activation to the "correct output" counts.  With such accuracies summarized (e.g. with a minimum) across all the tokens generated for a text, the network may be able to produce an accurate answer.

Something similar might be done for multi-digit arithmetic.  A network that has learned how to add four-digit numbers but is still learning how to add five-digit numbers might enable a digit node, but note that the output is still wrong. [Cite work on emergence]

## Potential mechanism for determining if a fact is derived from the prompt
The first two potential techniques looked at facts 'memorized' in the nodes of an LLM. 

A third technique that could be used to verify the output of a source is attention tracing. When a knowledgebase is embedded as a cache or as a document looked up by a RAG system and therefore used as part of the prompt for the LLM, the attention mechanism can be used to see if any words trace back with strong attention to the cache.  Perhaps when the cache is ignored in the textual output there is significantly less attention to it in the LLM generating that output.

# Anticipated complexities

## Tracing infrequent nodes to output

While it is easy to count how many updates are made to a node (and this could even be done at inference time by performing backprop along with the inference), it is harder to determine whether a given node is used to construct an output.

Certainly, certain ReLUs close to the node may never turn on, and hopefully this information alone could be enough to know whether a node influenced the output.

But in other cases, it may be nessary to turn off "weak" learning nodes to see if they influence the next-word decisions.  This could easily become cost-prohibitive if the number of "weak" nodes (those with small samples from the training set) that aren't immediately turned off by a ReLU near them is high.

Similarly, attention can be weak without being turned off entirely and tracing back attention to a corpus could be challenging if the weak attention is not clearly different from the strong attention.

## Linking references to concepts

It is possible that even for rare topics where the network is likely to err (such as, for GPT3.5, on Collatz cycles that do not converge to zero) that the number of references on this topic is quite high -- in the hundreds, thousands, or even millions, so that it is impossible to enumerate all of the sources even for infrequent nodes that lead to incorrect answers.  Thus perhaps the idea of tying citations to specific nodes in the network is ill-founded.

# Conclusion

These are certainly something that I would like to give a try!  It seems that they may have the potential to give a reasonable "score" of the network's confidence on common vs. rare topics, so that the network could "bow out" or provide a citation for information available from only a few sources.