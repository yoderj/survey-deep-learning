This section of the course contains narrative text, similar to what you might find in a blog post or textbook.

This is the smallest portion of the course.

There are already many good textbooks out there, like [d2l.ai](https://d2l.ai) and I don't intend to duplicate what is written in them.


