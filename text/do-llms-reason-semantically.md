An important open question is whether LLMs (like ChatGPT) are capable of reasoning semantically.  To be clear, we are not asking whether LLMs are reasoning in the sense that we reason as human beings.  Rather, we are asking one of the following questions:

* Do LLMs compute considering the semantic meanings of their text or only generate syntactically-correct regurgitations of memorized text?
* Are LLMs capable of generating novel arguments, or do they simply regurgitate arguments (perhaps with subtle surface variations) that they have found on the internet and in their large training sets?

These questions are important because we can easily imagine that the LLM is reasoning when it produces the text of a reasonable argument.  We know that when we talk to an LLM, the LLM is generating text and is not really alive.  But even accepting this fact, are we imagining that the LLM is doing something far more interesting than it actually is?

There has been discussion that video generation technologies can build ["world models"](https://openai.com/index/video-generation-models-as-world-simulators/).   It is easy to imagine that LLMs are also building and computing on some sort of model of the world, at least some of the time.

Do LLMs do arithmetic by modeling decimal numbers? Or do they simply memorize the correct answer to oodles and oodles of arithmetic problems?

Does an LLM that can provide steps for stacking objects in a stable configuration simply memorize stable configurations and regurgitate text that explains the stability of a configuration?  Or does it compute on the stability properties of items to be stacked?

While it is difficult to imagine experiments that would conclusively answer the question about object stacking, it may be possible to answer the question about arithmetic, for example by detecting carries or other logical internal mechanisms for adding such as some sort of approximate carry-lookahead algorithm.

If you are interested in exploring these sorts of research problems, please let me know.
