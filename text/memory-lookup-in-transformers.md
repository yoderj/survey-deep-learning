This paper from 2023 provides some interesting observations on the mechanisms of a transformer:

https://arxiv.org/pdf/2302.06461 - A Study on ReLU and Softmax in Transformer

# Core Insight
There are a number of interesting insights in this paper, but what I like most is the discussion on p. 2 of how a Transformer's Feed Forward Network (eq. 1) is very similar to its Attention Mechanism (Key-Value Memory).  They note that ReLU(X · W1 + b1) · W2 + b2  is very similar to Softmax(X K) V if you let W1 = K and W2 = V, and especially if you replace ReLU with Softmax!

# Brief interpretation of this insight
This is an interesting observation because it points out that the feed-forward network (in which transformers memorize large quantities of information) and the attention mechanism (in which transformers look over a long text and look up information) are in fact closely related mathematicaly. 

Both techniques take an input embedding, match it to "key" embeddings through a dot product operation, and then return a "value" embedding.

Both techniques use a nonlinearity (e.g. ReLU or softmax) to turn off "value" embeddings where the key does not match.

And both embeddings can be applied to a string of text by computing both keys and values from re-embeddings of that text. (See p. 3, Eq. 3 of the "Study on ReLU" paper we are reviewing here.)

# Applications of this insight
Inspired by this observation, the paper explores replacing the Feed Forward network with a Key-Value memory, and then replacing the attention mechanism with a sort of feed-forward network.

# Relationship to RAG
I have a feeling this sort of theoretical work can provide a perspective on RAG systems.  If one could see the entire RAG corpus as a large Key-Value Memory (that is, cross-attention from the query to the entire RAG corpus), and interact with that memory multiple times (as in [the first Key-Value memory paper](https://arxiv.org/pdf/1503.08895), which precedes the transformer by a couple of years), my guess is the accuracy of the RAG would be higher.  I have a gut feeling that current RAG systems inherently approximate such an idealized system in a variety of ways.

Let me try to say that in a different way.  Ideally, rather than using RAG systems, we would put the entire corpus as a single input into the transformer, with the query at the end. The transformer would then "self" attend from the query at the end back into the corpus to match relevant documents, revisiting the corpus in every layer of the network, and thus iterating between the corpus and the query in every layer of the network.  

Such an approach is clearly computationally prohibitive, yet it would resolve many common issues in RAG systems:  The query and the documents would be embedded into the same natural space and they would iterate until a strong match is achieved.

One challenge with RAG systems is finding the proper embedding for a document or a query.   Despite the impressive ability of a transformer to extend text, even transformers designed to embed a text to a single vector tend to perform poorly at embedding two texts on the same topic into nearly identical vectors.

A variety of approaches have been developed to try to overcome this.

For example, when embedding with a text-extending transformer, you can use prompts that end with a lead such as "A red house on the hill.  Describe the house:" rather than just "A red houe on the hill" such that the logits predicting the next word in the sentence will represent at least the first word of the description of the object.

Or, if using a model to embed an entire text to a single vector such as a [sentence transformer](https://www.sbert.net/), we might use an LLM to write a short fake article answering the question and then embed that fake article to match it to the real one.

The idealized "Transformer on whole RAG corpus" model naturally solves this problem (while being cost prohibitive).  As the query is extended word by word, this approach considers how to extend the query in the context of the entire corpus multiple times for each word, starting at the lowest layer of the network.  As the text is extended, it naturally reaches a point where a word needs to come from the corpus, at which point the attention mechanism has the entire prefix of the response already built to continue extending the answer.

The advantage of the "fake article", however, might not be achieved by the ideal "transformer on whole corpus" model unless some sort of chain-of-thought prompting were also included, in which the transformer was prompted to produce a fake article and then talk about a real article that it quotes.

An advantage of RAG over this idealized model is that a short subset of documents is pulled from the corpus, making it easier for a human to verify from the trusted corpus that the output of the model is a valid summary.  The "transformer on corpus" model might attend to many documents simultaneously and it may be difficult to trace its output to the corpus.

This challenge leads to another interesting aspect of the primary paper "A Study on ReLU and Softmax in Transformer" that we are exploring here -- how does a transformer memorize the internet, and how does it "learn" from its own input at inference time?"

# Relationship to prompt caching

Anthropic now offers [prompt caching](https://www.anthropic.com/news/prompt-caching), a service which (presumably) caches not only the prompt but also its embeddings in each layer of a model.  These then don't need to be recomputed for each conversation that extends that prompt.

Prompt caching further blurs the line between "memory" in a RAG system and "memory" in a prompt.  One can use either RAG or prompt caching for fairly efficient corpus retrieval.  There are still differences:

* It is likely still O(MN) in the attention layers, where M is the number of tokens in the prompt and N is the number of tokens in the query.  RAG is likely still more efficient.
* Prompt caching is closer to the ideal "transformer on entire corpus" idea discussed above.  Indeed, it IS an efficient approach to the "transformer on the entire corups" approach. Thus, it has the same strengths and the hallucination weaknesses of that approach.

# Memory lookup in transformers

As discussed in the section "Brief interpretation of this insight" above, a transformer performs two kinds of lookups:  Lookups on the text that is input to the transformer, and lookups into its feedforward layers.  In "A Study...", the authors point out that these two sorts of lookups support global and local memory, with the feed-forward layers looking up global knowledge and the attention mechanisms looking up local knowledge within the input text.

At inference time, every token of output in the transformer is derived from the embedded input tokens and the memorized embeddings within the feed-forward networks. (Each attention layer also memorizes a re-embedding of the inputs.)  By seeing both of these operations as a memory lookup, can we better understand how the network produces its output?

# Editing Transformer Memories

There is a large body of work on editing the memories stored in models. ["Mass Editing Memory in a Transformer"](https://memit.baulab.info/) is an example of one work where many memories can be looked up and corrected simultaneously (presumably to correct factual errors, not to promulgate propaganda!)

# Finding more connections
On a different topic brought up in this paper, in the past, N has commented that one of the strengths of Transformers is the giant fully-connected nature of these networks, with the ability to pick up on patterns between anything and anything.  This paper points out that ReLU can actually perform slightly better within the attention layers than Softmax, since Softmax tends to force only a few items to be turned on, while ReLU can turn on many items at the same time, assuming it is properly normalized.

