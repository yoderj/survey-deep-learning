---
title: "Narrative Text: Reflections on 'Elucidating the Design Space of Diffusion-Based Generative Models'"
---

In [Demystifying Diffusion-Based Models](../readings/technical/diffusion-ode.md), we look at a blog post based on the paper [Elucidating the Design Space of Diffusion-Based Generative Models](https://arxiv.org/pdf/2206.00364).  While the blog posts provides some good intuition into how neural networks can train (along with some fantatic animated plots), the paper itself provides a more rigorous definition of a class of denoising algorithms.

In this non-required reading, we reflect on some interesting conclusions one can draw from the paper itself to better understand the figures in the blog-post reading.

# Defining the noised distribution $p(\mathbf{x}; \sigma(t))$ as a convolution

Appendix B.3 starts with (top of p. 23, eq. 41), this 'definition' (which is actually derived as Eq. 20 from Appendix B.2):

$p(\mathbf{x}; \sigma(t)) = p_{data} * \mathcal{N}(0, \sigma(t)^2 \mathbf{I})$

Where $*$ indicates the convolution operation.  This makes sense because this is a way of expressing that every point in the input is spread out according to a normal distribution.

Also, interestingly, this is now exactly defined for all x and t, given an original distribution.  This gives us a good way to think intuitively about how likely we are to come to a random image $\mathbf{x}$ as the distribution $p(\mathbf{x}; \sigma(t))$ spreads with growing $\sigma(t)$ -- one that makes sense when looking at the orange distributions in the figures in the blog post and this paper -- each point simply grows outward as a Guassian until it's Gaussian overlaps and blends in with the Gaussians of other points.

# Why the ODE in Eq. (1) makes sense

Intuitively, for a single point with $\sigma(t) = t$, we know that the distribution flow should simply be linear from the true noise-free point $\mathbf{x}_0$ to any given 'random' point $\mathbf{x}$, so that the rate of change is simply the slope between these two points: $\mathrm{d}\mathbf{x}/\mathrm{d}t = (|\mathbf{x} - \mathbf{x}_0|/t) = (|\mathbf{x}_0 - \mathbf{x}|/\sigma)$.  We know this because, by definition, that is how the distribution is scaled out through time as noise is added -- the noise at time $2t$ is simply double the noise at time $t$. So the reverse distribution should have the same slope to remove the noise.  Subsituting (2), $\sigma(t) = t$, $\dot{\sigma}(t) = 1$, and $D(\mathbf{x},\sigma) = \mathbf{x}_0$ into (1) yields this formula.

The important thing is that this formula is very intuitive.  The network $D(\mathbf{x},\sigma)$ is responsible for estimating the unnoised image, which, in this example, is $\mathbf{x}_0$.  The denoising formula simply takes a direct line to the unnoised point from the noised point and follows that slope.

# Ideal denoising algorithm

As an aside in Appendix B.3, they derive equation $D(\mathbf{x},\sigma)$ = $\frac{\sum_i \mathcal{N}(\mathbf{x};\  \mathbf{y}_i,\ \sigma^2,\ \mathrm{\mathbf{I}})\mathbf{y}_i}{\sum_i \mathcal{N}(\mathbf{x};\ \mathbf{y}_i,\ \sigma^2,\ \mathrm{\mathbf{I}})}$ (Eq. 57 in Appendix B.3), which shows that the ideal denoising function is a weighted sum of all the samples that are near a point \mathbf{x} that is being denoised.  Intuitively, you can think of expanding a Gaussian distribution around the point, capturing all the $\mathbf{y}_i$ that have not-practically-zero probability, and weighting each of them according to the weight that that gaussian distribution gives them.

Indeed, this equation is the one used to generate Figure 1 on p. 2 of the paper, as it can be evaluted exactly for a given image by simply running that image through the entire training set using Equation 57 as the code. (See bottom of p. 23 in the paper.)

Understanding the ideal denoising algorithm is important to understanding why denoising diffusion works.  At first, noisy images are driven towards a mean image. But at some point, after $\sigma$ gets small enough, only some of the points are included in the "capture region" of the gaussians in Eq. 57.  At this point, $D(\mathbf{X},\sigma)$ guides towards just a subset of the training images that happen to be close to the random image selected.  As $\sigma$ decreases further, $D(\mathbf{X},\sigma)$ guides towards just a single image from the training set, or, ideally, a new image that interpolates between images found in the training set.

The astounding thing about denoising diffusion is NOT that it can denoise to images in the training set.  Understanding Eq. 57 above makes it clear that the algorithm should denoise to images within the training set: It is clear that as we reduce noise, eventually this will mean denoising to a single image within the training set.  Thus, the very definition of the denoising algorithm leans towards producing crisp, noise-free images.

The astounding thing **is** that a network trained to reconstruct the original images will not only collapse to images in the training set but interpolate between them in meaningful ways.  This is surprising because the network is trained on L2 error -- literally on pixel-wise comparisons of images.  

But this is less surprising if we consider the "inductive bias" (e.g., [2112.10752](https://arxiv.org/pdf/2112.10752), p. 4, bottom of left column) of a U-Net model. Because the model, by its very structure, is forced to learn local spatial dependencies, it is natural for it to produce images with similar local spatial properties when seeded with an appropriate random image.

Intuitively, we can imagine that during the training process, for novel random points not in the training set, the network gradually denoises to the point that no images from the training set are within the space that the network ought to generate, and that from that point on, it is the "inductive bias" of the network that drives the resulting denoised image to a good-looking image.

That this works is closely related to a concept we explore in the required deep learning class, that deep neural networks learn to interpolate between their training points in a way that leads to generalized images.  I have several more papers on this topic if you would like to read more about this.


# Derivation of the score function $\mathbf{\nabla}_x \log p(\mathbf{x},\sigma(t))$

Appendix B.3 derives equation (3), showing that a $D(\mathbf{x},\sigma)$ which has been trained to minimize Eq. 2 will meet Eq. (3): $(D(\mathbf{x},\sigma) - \mathbf{x})/\sigma^2$ is $\mathbf{\nabla}_x \log p(\mathbf{x},\sigma(t))$ (in Eq. 69 in the appendix, on p. 24).  This is a satisfying proof because it shows that using Eq. 1 and Eq. 3 as an algorithm is guaranteed to work to denoise points in the training set.

# Proof that the ODE in Eq. (1) is correct

In the earlier section, "Why the ODE in Eq. (1) makes sense", we provided an intuitive argument or "derivation" of the ODE for the case of $\sigma(t) = t$ and a single point of error.  It might be possible to expand that discussion into a proof of correctness for an arbitrary dataset by starting from the idealistic probability distribution considered in Appendis B.3, Eq. (40). However, that is not the approach taken by this paper to prove their ODE is correct.

Instead, Appendix B.2 (which starts on p. 13, and after a bunch of image pages, continues on page p. 21), is a derivation of their formula from formulae derived in prior work. While proving the correctness of their general ODE, this deriviation is not terribly useful for building intuition about WHY the ODE is correct.
