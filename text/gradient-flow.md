> **Spoiler alert**: If you are an MSOE student who is taking or plans to take CSC 4611, we will be 
> revealing the "answers" to some of the gradients you work on in class in that course here.
>
> If you want to work the exercises early, just ask Dr. Yoder!

Deriving backpropagation equations is a lot of work.  One of the main reasons for working through it is so that you can visualize how the data flows backward and forwards through the network.

The signal flows forward through the network. The gradient flows backwards through the network.
 
But some things stop the flow of information:

* A ReLU stops the flow of negative signals forward
* A max element stops the flow of all signals except the max forward

Similarly,

* A ReLU stops the gradient from flowing backwards whenever the input is negative. (But it will let negative gradients flow backwards!)
* A max element only flows backwards the gradient to the maximum input. All other inputs receive no gradient (only a zero gradient).

We have seen that the backprop equation for a linear layer is also linear -- with the gradient values essentially flowing backwards through the same weights that the signal went forwards through. (This is what transforming the weights does.)  So we know that if the weights increase the signal in the forward direction, they will also increase the gradients as they flow in the backwards direction (at least for a nearly-square matrix).

Being able to visualize or reason through how these gradients flow can help explain some details of how a neural network trains.  For example, when training CNNs, we sometimes see speckle patterns that aren't in the original image. Since the backprop of a max pool is speckley, this might partly explain where these patterns come from.

Similarly, understanding how a linear layer increases or decreases the signal flowing through it can help us to stabilize the numerical values of the forward signals and backward gradients flowing through our networks.
