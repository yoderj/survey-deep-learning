CSC-4980 Syllabus
------------

# Grade Breakdown

| Topic         | Perc.  |
|---------------|--------|
| Readings      | 35%    |
| Quizzes       | 5%     |
| Half-Exams    | 30%    |
| Final Exam    | 30%    |

There will be three Half-Exams.

Each half-exam will constititute 10% of the course grade.

Topics
------

This will be an OUTCOMES-driven course.  And YOU will have an influence on what the outcomes are.

I will provide about half of the outcomes for the course, and you, as a class, will be responsible for determining the remaining outcomes.

My main goal in this course is to guide you into *understanding* how the most capable neural networks available today work.  I also want to guide you into *caring* about how neural networks impact people and the planet.  But neither of these goals can be measured through exams or quizzes.  I will *not* assess either of these directly.

Instead, I will use *learning outcomes* -- activities you can perform on exams and quizzes to demonstrate your knowledge of the material we are covering in this course.  A learning outcome consists of two key parts: A ***verb*** describing an action you can perform on an exam or quiz within the context of a *broader description* of that goal.  Here are some examples:

* *Illustrate* the sliding motion of a filter whose weights are shared by the output of a convolutional layer.
* *Describe* where the training data goes into a fully-connected layer's weights
* *Explain* the essential role of GPUs in training deep networks

Notice that these learning outcomes cannot be placed directly onto an exam or a quiz.  Instead, they describe a skill that you could demonstrate if the instructor set up the question appropriately.  In general, we will be able to discuss between three to six outcomes in a given class period.  

Office Hours
------------

# Pre-course Survey

1. Which inspires you to study deep learning the most?
  1. ChatGPT and other chat agents
  2. Stable Diffusion and other image-generation agents
  3. Medical imaging and other healthcare applications
  4. Other: ___________________

2. How comfortable are you to be able to get something out of academic research papers by reading them?

  1 - I don't think I'll ever be able to do it.
  5 - I can already do it.

3. How comfortable are you in *understanding* how deep networks work?

4. Can you multiply a matrix by a vector, given the values of the elements?

  1 - What does that even mean?
  5 - Give me the numbers!

5. Do you think that deep learning networks will change society positively or negatively, in net?

6. (Unused Spring 2024) What do you think your role in developing systems using deep learning in the future will be?


# Exercise for Class 1-2 (Week 1, Class 2)

1. Write one outcome for something you would like to be able to do at the end of this course.  It does NOT need to be restricted to something that you can do on an exam or a quiz. But it should be something you can *do*, not simply understand, like the examples we discussed in class.
