# gemma model walkthrough

Throughout this term, several students have expressed an interest in seeing real code for a transformer.

[A code walk-through of Google's Gemma transformer](https://graphcore-research.github.io/posts/gemma/) was publicized in Week 15 by TLDR.  It is written by Douglas Orr.

You may be interested in this walk-through because it is accompanied by a [Google Colab](https://github.com/DouglasOrr/DouglasOrr.github.io/blob/examples/2024-04-transformers/gemma_walkthrough.ipynb) where you can run the code and play with it to see how things work.

My hope is that you will find much of the discussion familiar (except for RoPE -- an alternative positional embedding that most cutting-edge transformers use today) after our discussions from this term.  But the walk-through page (not colab) includes expandable sections with theory discussion in addition to the text.

# Getting started with the Colab

To get started with the [Google Colab notbook for this walkthrough](https://github.com/DouglasOrr/DouglasOrr.github.io/blob/examples/2024-04-transformers/gemma_walkthrough.ipynb), you need to create a HuggingFace account (if you don't have one), then go to the [gemma-2b hugging face model page](https://huggingface.co/google/gemma-2b) and agree to the terms to get access to Gemma (it's not a completely open-source model, you see).  Look for a large panel near the top of the main text with a large button on it to request access.

You also need to [create an access token](https://huggingface.co/settings/tokens) within your HuggingFace account. In the notebook, click on the key on the left panel, and add a new secret named `HF_TOKEN` whose value is your hugging face token that you copied after creating it.

You probably don't need this code, but I don't have time to test this at the moment. So if the above don't succeed in getting the model to download, try pasting this as a cell in your notebook:

```
from google.colab import userdata
import os
os.environ['HF_TOKEN'] = userdata.get('HF_TOKEN')
```

# Playing with the notebook

The notebook includes a from-scratch implementation of the forward pass of the model.  You can put in your own text to see what the next token the model predicts would be.  Just after this, the model checks that the from-scratch version produces exactly the same output as the HuggingFace version.

I haven't played with it much yet myself, but I found that in the "understanding plots", I could skip directly to Attention and have it plot the self-attention for my sequence without needing to run the other Part C "understanding plots" before that section first.

My guess is that this notebook might be a good playground for playing with a pretrained transformer

# Limitations

This notebook does NOT show you how to train a transformer, or how to do human-guided reinforcement fine-tuning, a topic beyond the scope of what we have learned about transformers together this term.
