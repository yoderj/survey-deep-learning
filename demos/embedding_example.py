import numpy as np


def softmax(x,axis=1):
    return np.exp(x)/(np.exp(x)).sum(axis=axis,keepdims=True)


if __name__ == '__main__':
    wordpiece = {'apple': 0,
                 'orange': 1,
                 'banana': 2,
                 'phone': 3,
                 'tablet': 4,
                 'buy': 5,
                 'please': 6,
                 'a': 7,
                 'I': 8,
                 'have': 9,
                 'an': 10,
                 'and': 11}

    pass

    embeddings = np.array(
        [[1.4, 1.4], [0, 2], [0, 2.1], [2, 0], [2.1, 0.1], [-2, 0], [-2, -2], [0, -2], [-2, -1], [-1, -2], [-2, -2.2],
         [-1.8, 2]])
    sentences = ['please buy an apple phone',
               'I have an apple and an orange',
               'please buy a banana and an apple',
               'I have an apple tablet',
               'I have an apple and an orange and a phone',
               'I have an orange',
               'an orange I have']


    def attention_layer(sentence):
        sentence.split(' ')
        words = sentence.split(' ')
        input_embedding = np.zeros([len(words), embeddings.shape[1]])
        for i, word in enumerate(words):
            input_embedding[i, :] = embeddings[wordpiece[word], :]

        pass
        # TO REDO: Softmax
        q = input_embedding
        k = input_embedding
        # A = softmax(q @ k.T, axis=1)
        v = input_embedding
        # output = A @ v
        # print('A without specialized embeddings:')
        # print(np.round(A, decimals=2))
        wq = 0.7 * np.array([[1, -1], [1, 1]])
        q @ wq
        pass
        wk = 0.7 * np.array([[1, 0.1], [1, 0.1]])
        k @ wk
        (q @ wq) @ (k @ wk).T
        A = softmax((q @ wq) @ (k @ wk).T, axis=1)
        # print('A without specialized embeddings:')
        # print(np.round(A, decimals=2))
        wv = 0.7 * np.array([[1, 0.5], [-1, 0.5]])
        v @ wv
        phone_out = A @ (v @ wv)
        print('*** Attention layer on ' + sentence + '***')
        print(phone_out)
        return phone_out

    for sentence in sentences:
        attention_layer(sentence)

pass
