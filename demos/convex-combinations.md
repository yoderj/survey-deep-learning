These desmos demos illustrate convex combinations:

* [Convex combinations of two points](https://www.desmos.com/calculator/z8i0emi8cp)
* [Convex combinations of three points](https://www.desmos.com/calculator/v1cmjdcuui)
* [Convex combinations of four points](https://www.desmos.com/calculator/cwt3pddfsd)
