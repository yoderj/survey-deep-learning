# Motivated by https://arxiv.org/pdf/2006.11239.pdf
# Section 3.2, first paragraph:
# "The first choice is optimal for x0 ∼ N (0, I), and the
# second is optimal for x0 deterministically set to one point."
#
# This code illustrates how the backwards distribution from a point
# towards a point can nevertheless have a non-zero standard deviation,
# and why this is useful from an "image diversity" perspective.
#
# Josiah Yoder. Spring 2024.

import numpy as np
import matplotlib.pyplot as plt

# d-dimensional point
point = np.array([[0,0]])
num_samples = 500
num_time_steps = 10
samples = point + np.zeros((num_samples,1))

point = np.array([[1,0]])
# num_samples = 50
num_time_steps = 10
samples2 = point + np.zeros((num_samples,1))

num_samples = 2 * num_samples
samples = np.concatenate((samples, samples2),axis=0)

# Broadcast out to multiple samples drawn from this distribution
# samples is n x d. n -- number of samples

# samples_through_time is T x n x d. T -- number of time-steps
samples_through_time = np.zeros((num_time_steps,num_samples,point.shape[1]))

plt.plot(samples[:, 0], samples[:, 1], '.')
samples_through_time[0,:,:] = samples
noise = 0.2
for i in range(0, num_time_steps-1):
    samples_through_time[i+1,:,:] = samples_through_time[i,:,:] + noise * np.random.randn(samples.shape[0],samples.shape[1])


# fig = plt.figure(figsize=(12, 12))
# ax = fig.add_subplot(projection='3d')
#
# ax.scatter(samples_through_time[0,:,:]+np.arange(0,num_time_steps), sequence_containing_y_vals, sequence_containing_z_vals)

pass

plt.figure()
plt.title('All lines')
plt.plot(samples_through_time[:,:,0].squeeze())

pass

plt.figure()
timestep = 8
plt.title('Lines going through 0.5 at timestep '+str(timestep))
interesting = (samples_through_time[timestep,:,0] > 0.47) & (samples_through_time[timestep,:,0] < 0.53)
plt.plot(samples_through_time[:,interesting,0].squeeze())

pass