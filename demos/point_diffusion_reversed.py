# Motivated by https://arxiv.org/pdf/2006.11239.pdf
# Section 3.2, first paragraph:
# "The first choice is optimal for x0 ∼ N (0, I), and the
# second is optimal for x0 deterministically set to one point."
#
# This code illustrates how the backwards distribution from a point
# towards a point can nevertheless have a non-zero standard deviation,
# and why this is useful from an "image diversity" perspective.
#
# The denoising algorithm is inspired by
# https://developer.nvidia.com/blog/generative-ai-research-spotlight-demystifying-diffusion-based-models/
# You just need to read the whole article...
#
# Josiah Yoder. Spring 2024.

import torch
import matplotlib.pyplot as plt
import matplotlib
import scipy.stats # only need for 'optimal' strategy

# With this block, we don't need to set device=DEVICE for every tensor.
# Also don't need to set dtype to float for every tensor.
torch.set_default_dtype(torch.float32)
if torch.cuda.is_available():
     torch.cuda.set_device(0)
     torch.set_default_tensor_type(torch.cuda.FloatTensor)
     print("Running on the GPU")
else:
     print("Running on the CPU")

input_distribution = 'uniform' # 'normal' # 'point' #
print('Input distribution:',input_distribution)
use_conditioning = False
denoising_strategy = 'optimal' # 'nn' #
num_samples = 1000
num_time_steps = 10
# time_steps: See eq 5, pg., 5, of https://arxiv.org/pdf/2206.00364.pdf (NVIDIA diffusion paper)
sigma_min = 0.001
sigma_max = 2
p = 7 # rho from the paper
step_indices = num_time_steps - 1 - torch.arange(0, num_time_steps, dtype=torch.float32)
sigmas = torch.cat((torch.tensor([0]),
                        (sigma_max**(1/p)
                             + step_indices[1:None]/(num_time_steps-1)*(sigma_min**(1/p)-sigma_max**(1/p)))**p))
time_steps = sigmas
num_dimensions = 2  # This parameter is not adjustable, and only one of the dimensions is
# visualized in much of this code.
NUM_EPOCHS = 30
np_newaxis = None # This indicates to create a new size-1 dimension at wherever this varible is used as an index.

def sample_from_input_distribution(num_samples, anchor):
    """
    :param num_samples:
    :param anchor: Mean of normally-distrubited date, minimum of uniform data
    :return:
    """
    if input_distribution == 'point':
        samples = anchor + torch.zeros((num_samples, num_dimensions))
    elif input_distribution == 'normal':
        input_sigma = 0.2
        samples = anchor + input_sigma * torch.randn((num_samples, num_dimensions))
    elif input_distribution == 'uniform':
        samples = anchor + torch.rand((num_samples, num_dimensions))
    else:
        raise Exception('Unknown input distribution:', input_distribution)
    return samples


# d-dimensional point
# First mode of original data
assert num_dimensions == 2 # The anchor points are defined in two dimensions
point = torch.tensor([[0,0]])
samples = sample_from_input_distribution(num_samples//2, point)

# Second mode of original data
assert num_dimensions == 2 # The anchor points are defined in two dimensions
point = torch.tensor([[1,0]])
# num_samples = 50
samples2 = sample_from_input_distribution(num_samples//2, point)

samples = torch.cat((samples, samples2),axis=0)
sample_class = torch.cat([torch.ones(num_samples//2), torch.zeros(num_samples//2)])

# Broadcast out to multiple samples drawn from this distribution
# samples is n x d. n -- number of samples

# samples_through_time is T x n x d. T -- number of time-steps
samples_through_time = torch.zeros((num_time_steps,num_samples,num_dimensions))

samples_through_time[0,:,:] = samples
for i in range(1, num_time_steps):
    samples_through_time[i,:,:] = samples_through_time[0,:,:] + sigmas[i] * torch.randn(num_samples, num_dimensions)

# fig = plt.figure(figsize=(12, 12))
# ax = fig.add_subplot(projection='3d')
#
# ax.scatter(samples_through_time[0,:,:]+np.arange(0,num_time_steps), sequence_containing_y_vals, sequence_containing_z_vals)

pass

plt.figure(1) # Will come back to this figure later
plt.title('All lines')
# [:,:,0] -- looking only at first dimension of each 2-d sample. Second dimension not visualized in much of this code
plt.plot(time_steps,samples_through_time[:,:,0].squeeze(),alpha=0.2)

pass

if denoising_strategy == 'nn': # Neural Network (nn)
# Create a training data linking every point in Figure 1 back to the start of its curve
# The first step is cut off because (I speculate) it doesn't make sense to train the network on no-noise data
# and may even hurt the network's training.
#
# To cut off the first, unnoisy sample, we use 1:None slicing.
# 1:None means index one through the last sample. (Can't do 1:-0 because that would be the same as 1:0)
# Can also use [1:] (with nothing after the :, but hard to see that and easy to forget the : entirely that way.
    start_index = 0 # keep unnoised data # 1 # remove first timestep (unnoised data) #
    num_training_samples = (num_time_steps-start_index)*num_samples
    flattened_noise_samples = samples_through_time[start_index:None,:,:].reshape([num_training_samples, num_dimensions])
    expanded_orig_samples = (samples[np_newaxis,:,:] + torch.zeros((samples_through_time.shape[0],1,1)))
    flattened_orig_samples  = expanded_orig_samples[start_index:None,:,:].reshape([num_training_samples, num_dimensions])
    small_time = torch.arange(0,num_time_steps)[:,np_newaxis]
    expanded_time = small_time + torch.zeros((num_time_steps,num_samples))
    flattened_time = expanded_time[start_index:None,:].reshape([num_training_samples, 1])
    expanded_class = sample_class[np_newaxis,:] + torch.zeros((num_time_steps,num_samples))
    flattened_class = expanded_class[start_index:None,:].reshape([num_training_samples, 1])


    if not use_conditioning:
        inputs = torch.cat((flattened_noise_samples,flattened_time),axis=1)
    else:
        inputs = torch.cat((flattened_noise_samples,flattened_time,flattened_class),axis=1)

    outputs = flattened_orig_samples

    idx = torch.randperm(inputs.shape[0])
    inputs = inputs[idx,:]
    outputs = outputs[idx,:]

    # Model to be trained. The three inputs are x1, x2, and time. The three outputs are x1 and x2 of the point
    # at the start of the curve (only x1 is shown in the figures)
    model = torch.nn.Sequential(
        torch.nn.Linear(3, 16) if not use_conditioning else torch.nn.Linear(4, 16),
        torch.nn.ReLU(),
        torch.nn.Linear(16, 16),
        # torch.nn.ReLU(),
        # torch.nn.Linear(16, 16),
        torch.nn.ReLU(),
        torch.nn.Linear(16, 2)
    )

    # Train the model.
    # SGD updates every weight according to  W = W_old - lr * gradients. Gradients will be computed by .backward()
    optimizer = torch.optim.SGD(model.parameters(),lr = 0.01, momentum=0.5)

    # MSE == mean square error.  Simply mean of the squares of the error for each coordinate. Mean across a batch.
    loss_fn = torch.nn.MSELoss()

    # Number of times to pass over the entire training set
    if not use_conditioning:
        model_filename = 'trained-'+input_distribution+'.pickle'
    else:
        model_filename = 'conditioned-'+input_distribution+'.pickle'
    try:
        model = torch.load(model_filename)
        print('Loaded a previously-saved model:'+model_filename)
    except:
        print('Could not load model. Training again.')
        # Main training loop
        # https://pytorch.org/tutorials/beginner/introyt/trainingyt.html#the-training-loop
        for epoch in range(1, NUM_EPOCHS + 1):
            print('epoch:',epoch)
            for input, output in zip(inputs, outputs):
                optimizer.zero_grad()

                sample = model.forward(input)
                loss = loss_fn(sample, output)
                loss.backward()

                optimizer.step()
        print('Saving model to'+model_filename)
        torch.save(model, model_filename)
elif denoising_strategy == 'optimal':
    pass # Nothing to train as we will denoise from whole initial distribution
else:
    raise Exception('Unknown denoising strategy strategy'+denoising_strategy)


def denoise_one_step(estimates, x_start, time_start, time_end, last_step=False):
    """
    Perform one step of diffusion
    :param estimates: Model denoised estimate
    :param x_start: Place from which each point was diffuxed
    :return:
       end: The ending point of the step, [time, x_1] only. (Ignores x2)
       start: The starting point of the step, [time, x_1] only. (Ignores x2)
    """
    x_fully_denoised = estimates
    if time_start == 0:
        raise Exception('Should not start denoising from zero noise')
    if last_step:
        alpha = 0
    else:
        alpha = time_end/time_start
    x_end = alpha*x_start + (1-alpha)*x_fully_denoised
    return x_end


# Show path of denoising process (black lines)
plt.figure(1)
middle = 1
spread = 10
x_start = torch.arange(middle-spread,middle+spread+0.1,1,dtype=torch.float32)
x_start = torch.cat((x_start.flatten()[:,np_newaxis], torch.zeros((len(x_start),num_dimensions-1))),axis=1)

# When one_step == True, each step jumps to denoised image in one step.
one_step = True # False #
if one_step:
    lines = None

for i in range(time_steps.shape[0]-1, 0, -1):
    if one_step and lines is not None:
        for line in lines:
            line.set_alpha(0.1)
    time_start = time_steps[i:i + 1] * torch.ones(x_start[:, 0:1].shape)
    if not use_conditioning:
        samples = torch.cat((x_start, time_start),axis=1)
    else:
        # Condition on class zero only
        samples = torch.cat((x_start, time_start, torch.zeros((x_start.shape[0], 1))), axis=1)
    if denoising_strategy == 'nn':
        estimates = model.forward(samples).detach()
    elif denoising_strategy == 'optimal':
        # See https://arxiv.org/pdf/2206.00364 (Elucidating...), p. 23, Eq. 57
        original_samples = samples_through_time[0, :, :]
        logs = scipy.stats.norm.logpdf(x_start[np_newaxis, :, :], original_samples[:, np_newaxis, :], time_start[0, 0])
        weights = torch.exp(torch.tensor(logs.sum(axis=2)))
        weighted_samples = (weights.T.float() @ original_samples)
        sample_weights = weights.sum(axis=0, keepdims=True)
        estimates = weighted_samples / sample_weights.T
    else:
        raise Exception('Unknown denoising strategy')
    if one_step:
        x_new = denoise_one_step(estimates, x_start, time_steps[i], 0*time_steps[i - 1])
        time_new = 0 * time_steps[i-1:i] * torch.ones(x_start[:,0:1].shape)
    else:
        x_new = denoise_one_step(estimates, x_start, time_steps[i], time_steps[i - 1])
        time_new = time_steps[i-1:i] * torch.ones(x_start[:,0:1].shape)
    time_coords_line = torch.cat((time_start, time_new), axis=1)
    x_coords_line = torch.cat((x_start[:,0:1], x_new[:,0:1]), axis=1)
    lines = plt.plot(time_coords_line.T,x_coords_line.T,color='k', marker='.', linewidth=1, markersize=1.5)
    x_new = denoise_one_step(estimates, x_start, time_steps[i], time_steps[i - 1])
    x_start = x_new


plt.ylabel('x1')
plt.xlabel('time')
plt.title(input_distribution + (' (conditional)' if use_conditioning else ''))

pass