import torch
import pandas as pd

INITIAL_WEIGHT_SCALE = 0.5

NUM_EPOCHS = 800 # Number of passes over the training set.
LEARNING_RATE = 0.1 # Step size to change weights by on each weight update
INPUT_EMBEDDING_SIZE = 4
HEAD_EMBEDDING_SIZE = 2
NUM_HEADS = 2

# With this block, we don't need to set device=DEVICE for every tensor.
torch.set_default_dtype(torch.float32)
if torch.cuda.is_available():
     torch.cuda.set_device(0)
     torch.set_default_tensor_type(torch.cuda.FloatTensor)
     print("Running on the GPU")
else:
     print("Running on the CPU")


def softmax(x,axis=1):
    return torch.exp(x)/(torch.exp(x)).sum(axis=axis,keepdims=True)


def attention_network(input_text, output_text=None, *, wordpiece, embeddings, wq, wk, wv, w_out, debug=False):
    words = input_text.split(' ')
    input_embedding = torch.zeros([len(words), embeddings.shape[1]])
    for i, word in enumerate(words):
        input_embedding[i, :] = embeddings[wordpiece[word], :]

    # TO REDO: Softmax
    num_heads = wq.shape[2] # size of third dimension of embeddings
    output_embedding = torch.zeros(len(words),wq.shape[1],wq.shape[2])
    for i in range(num_heads):
        q = input_embedding @ wq[:,:,i]
        k = input_embedding @ wk[:,:,i]
        v = input_embedding @ wv[:,:,i]
        A = softmax(q @ k.T, axis=1)
        output_embedding[:,:,i] = A @ v
    output_embedding = output_embedding.reshape([len(words),wq.shape[1]*wq.shape[2]])
    if debug:
        print('*** Attention layer on ' + input_text + '***')
        print(output_embedding)
    output_embedding.retain_grad()
    loss = torch.nn.CrossEntropyLoss()

    # Output layer
    class_values = output_embedding @ w_out
    class_values.retain_grad()

    # True output
    if output_text is not None:
        output_words = output_text.split()
        true_output_index = torch.zeros(len(output_words), dtype=torch.long)
        for i, word in enumerate(output_words):
            true_output_index[i] = wordpiece[word]

        sample_loss = loss(class_values, true_output_index)
        sample_loss.backward()
        pass

    return class_values


if __name__ == '__main__':
# def tmp(): # For PyCharm refactoring
    wordpiece = {'Apple': 0,
                 'apple': 1,
                 'orange': 2,
                 'banana': 3,
                 'phone': 4,
                 'tablet': 5,
                 'buy': 6,
                 'please': 7,
                 'a': 8,
                 'i': 9,
                 'I': 10,
                 'have': 11,
                 'an': 12,
                 'and': 13,
                 'eating':14,
                 'like':15}

    vocabulary_size = len(wordpiece)
    embeddings = INITIAL_WEIGHT_SCALE * torch.randn((vocabulary_size, INPUT_EMBEDDING_SIZE))
    embeddings.requires_grad_(True)

    wq = INITIAL_WEIGHT_SCALE * torch.randn((INPUT_EMBEDDING_SIZE, HEAD_EMBEDDING_SIZE, NUM_HEADS))
    wq.requires_grad_(True)
    wk = INITIAL_WEIGHT_SCALE * torch.randn((INPUT_EMBEDDING_SIZE, HEAD_EMBEDDING_SIZE, NUM_HEADS))
    wk.requires_grad_(True)
    wv = INITIAL_WEIGHT_SCALE * torch.randn((INPUT_EMBEDDING_SIZE, HEAD_EMBEDDING_SIZE, NUM_HEADS))
    wv.requires_grad_(True)
    w_out = INITIAL_WEIGHT_SCALE * torch.randn((HEAD_EMBEDDING_SIZE * NUM_HEADS, vocabulary_size))
    w_out.requires_grad_(True)

    # output_sentences = ['an Apple phone',
    #                     'apple and an orange',
    #                     'a banana and an apple',
    #                     'an Apple tablet']
    output_sentences = ['please buy an Apple phone',
                        'I have an apple and an orange',
                        'please buy a banana and an apple',
                        'I have an Apple tablet',
                        'I like eating an apple and an orange']

    input_sentences = [x.lower() for x in output_sentences]

    for epoch in range(1, NUM_EPOCHS+1):
        for input, output in zip(input_sentences, output_sentences):
            attention_network(input_text=input, output_text=output, wordpiece=wordpiece, embeddings=embeddings,
                              wq=wq, wk=wk, wv=wv, w_out=w_out)
            embeddings.data -= LEARNING_RATE * embeddings.grad
            wq.data -= LEARNING_RATE * wq.grad
            wk.data -= LEARNING_RATE * wk.grad
            wv.data -= LEARNING_RATE * wv.grad
            w_out.data -= LEARNING_RATE * w_out.grad
            embeddings.grad.zero_()
            wq.grad.zero_()
            wk.grad.zero_()
            wv.grad.zero_()
            w_out.grad.zero_()

            pass

    for input, output in zip(input_sentences, output_sentences):
        class_values = attention_network(input_text=input, output_text=output, wordpiece=wordpiece, embeddings=embeddings,
                          wq=wq, wk=wk, wv=wv, w_out=w_out)

        out = pd.DataFrame(softmax(class_values, axis=1).round(decimals=2).detach(), columns=wordpiece.keys())

        inverse_wordpiece = {}
        for k, v in wordpiece.items():
            inverse_wordpiece[v] = k

        estimated_output = ' '.join([inverse_wordpiece[x] for x in softmax(class_values).argmax(axis=1).tolist()])

        print()
        print('Input:           ',input)
        print('Correct Output:  ',output)
        print('Estimated Output:',estimated_output)

    # test_sentences = ['an Apple tablet',
    #                     'an apple and a banana',
    #                     'a banana and an apple',
    #                     'an Apple phone']
    test_sentences = ['please buy an Apple tablet',
                      'I have an apple and a banana',
                      'please buy an orange and an apple',
                      'I have an Apple phone',
                      'I like eating a banana and an apple']
    test_input_sentences = [x.lower() for x in test_sentences]
    for input in test_input_sentences:
        class_values = attention_network(input_text=input, output_text=None, wordpiece=wordpiece, embeddings=embeddings,
                                         wq=wq, wk=wk, wv=wv, w_out=w_out)

        out = pd.DataFrame(softmax(class_values, axis=1).round(decimals=2).detach(), columns=wordpiece.keys())

        inverse_wordpiece = {}
        for k, v in wordpiece.items():
            inverse_wordpiece[v] = k

        estimated_output = ' '.join([inverse_wordpiece[x] for x in softmax(class_values).argmax(axis=1).tolist()])

        print()
        print('Novel Input:     ', input)
        print('Estimated Output:', estimated_output)

    # Now explore the embedding
    import matplotlib.pyplot as plt
    import numpy as np

    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')

    x = embeddings[:,0].detach()
    y = embeddings[:,1].detach()
    z = embeddings[:,2].detach()
    c = embeddings[:,3].detach()

    img = ax.scatter(x, y, z, c=c, cmap=plt.hot())
    for i in range(0,x.shape[0]):
        ax.text(x[i],y[i],z[i],inverse_wordpiece[i])
    fig.colorbar(img)

    for i in range(0,2):
        q = embeddings @ wq[:, :, i]
        k = embeddings @ wk[:, :, i]
        plt.figure()
        plt.title('Head '+str(i))
        plt.scatter(q[:,0].detach(),q[:,1].detach(),color='k')
        plt.scatter(k[:,0].detach(),k[:,1].detach(),color='r')
        for i in range(0, x.shape[0]):
            plt.text(q[i,0].detach(),q[i,1].detach(), inverse_wordpiece[i])

        for i in range(0, x.shape[0]):
            plt.text(k[i,0].detach(),k[i,1].detach(), inverse_wordpiece[i],color='r')

    # plt.show() # When in IntelliJ Debug, run this from the debug console to avoid
                 # locking up the screen.
    pass
    ignored = 'please stop here'